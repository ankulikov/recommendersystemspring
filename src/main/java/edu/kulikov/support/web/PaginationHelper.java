package edu.kulikov.support.web;

import edu.kulikov.view_model.Page;
import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Andrey on 02.03.2015.
 */
public class PaginationHelper {

    private int currentPage;
    private int itemsPerPage;
    private long totalItems;

    private int totalPages;


    public PaginationHelper(Integer currentPage, int itemsPerPage, long totalItems) {
        this.currentPage = currentPage == null ? 0 : currentPage;
        this.itemsPerPage = itemsPerPage;
        this.totalItems = totalItems;
        this.totalPages = (int) Math.ceil((double) this.totalItems / this.itemsPerPage);
    }

    public int getOffset() {
        return currentPage * itemsPerPage;
    }

    public int getCurrentPage() {
        return currentPage;
    }

    public int getItemsPerPage() {
        return itemsPerPage;
    }

    public long getTotalItems() {
        return totalItems;
    }

    public int getTotalPages() {
        return totalPages;
    }

    public List<Page> getPagination(int pagesToShow) {
        //pages starts from 0
        List<Page> pageList = new ArrayList<>(pagesToShow);
        //first page index without "<<" and ">>"
        Pair<Integer, Integer> paginationBounds = pageGroupStartPage(pagesToShow - 2);
        //previous page
        pageList.add(new Page("<<", ""+(currentPage - 1), false, currentPage != 0));
        //digit pages
        for (int i = paginationBounds.getLeft(); i<= paginationBounds.getRight(); i++) {
            pageList.add(new Page(String.valueOf(i), "" + i, currentPage == i, true));
        }
        //next page
        pageList.add(new Page(">>", "" + (currentPage + 1),
                false, currentPage != totalPages - 1)); //if last page then inactive
        return pageList;
    }


    //Pair<start index, end index>
    private Pair<Integer, Integer> pageGroupStartPage(int pagesInPageGroup) {
        int firstIndex;
        int lastIndex;
        if (currentPage == 0 || currentPage == 1) {
            firstIndex = 0;
            lastIndex = Math.min(currentPage - 1 + pagesInPageGroup, totalPages - 1);
        } else {
            firstIndex = currentPage - 2;
            lastIndex = Math.min(currentPage - 3 +pagesInPageGroup, totalPages - 1);
        }
        return new ImmutablePair<>(firstIndex, lastIndex);
//
//        int totalPageGroups = (int) Math.ceil((double) totalPages / pagesInPageGroup);
//        boolean isLastGroupFull = totalPages % totalPageGroups == 0;
//
//        int currentPageGroupIndex = currentPage / pagesInPageGroup;
//
//        //return start position of group
//        if (currentPageGroupIndex != totalPageGroups -1 || isLastGroupFull)
//            return currentPageGroupIndex * pagesInPageGroup;
//
//        //last group is not full: return pages from the end
//        return totalPages  - pagesInPageGroup;
    }


}
