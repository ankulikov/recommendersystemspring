package edu.kulikov.support.web;

public class ValidationHelper {
    public static int checkPage(String page) {
        int pageId = 0;
        try {
            pageId = Integer.parseInt(page);
        } catch (NumberFormatException ignored) {
        }
        pageId = pageId < 0 ? 0 : pageId;
        return pageId;
    }

    public static int checkId(String id) {
        int intId = 1;
        try {
            intId = Integer.parseInt(id);
        } catch (NumberFormatException ignored) {
        }
        intId = intId <1 ?1: intId;
        return intId;
    }
}
