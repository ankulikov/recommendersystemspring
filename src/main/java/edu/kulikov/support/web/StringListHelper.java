package edu.kulikov.support.web;

import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by Andrey on 02.03.2015.
 */
public class StringListHelper {
    public static String transform(List<String> list) {
        return list.stream().collect(Collectors.joining(", "));
    }
}
