package edu.kulikov.bayes;


import edu.kulikov.models.*;
import edu.kulikov.services.MovieService;
import edu.kulikov.services.UserService;
import org.apache.commons.lang3.tuple.ImmutablePair;
import org.springframework.beans.factory.annotation.Autowired;

import java.io.*;
import java.util.EnumSet;
import java.util.List;
import java.util.stream.Collectors;

@SuppressWarnings("SpringJavaAutowiredMembersInspection")
public class BayesDataCreator {
    @Autowired
    private UserService userService;
    @Autowired
    private MovieService movieService;

    private String trainDirName;
    private String trainFileName;
    private String testDirName;
    private String testFileName;

    public BayesDataCreator(String trainDirName, String trainFileName, String testDirName,
                            String testFileName) {
        this.trainDirName = trainDirName;
        this.trainFileName = trainFileName;
        this.testDirName = testDirName;
        this.testFileName = testFileName;
    }

    public File exportRatedMovies(Integer userId, boolean useCache) throws IOException {
        ImmutablePair<File, Boolean> pair = createPathFile(trainDirName, trainFileName, userId, "xrff");

        //either forced creation or file is new
        if (!useCache || pair.getRight()) {

            try (BufferedWriter fileWriter =
                         new BufferedWriter(new OutputStreamWriter(new FileOutputStream(pair.getLeft()), "UTF-8"))) {
                User user = userService.getById(userId, EnumSet.of(User.LAZY.RATINGS));
                System.out.println(user.getRatings().size());
                writeHeader(false, fileWriter);
                writeRatedMovies(user.getRatings(), fileWriter);
            }
        }
        return pair.getLeft();
    }

    public File exportUnratedMovie(Integer movieId) throws IOException {
        ImmutablePair<File, Boolean> pair = createPathFile(testDirName, testFileName, movieId, "xrff");
        //if file was already existed then don't rewrite it
        if (pair.getRight()) {
            try (BufferedWriter fileWriter =
                         new BufferedWriter(new OutputStreamWriter(new FileOutputStream(pair.getLeft()), "UTF-8"))) {
                Movie movie = movieService.getById(movieId, EnumSet.allOf(Movie.LAZY.class));
                writeHeader(true, fileWriter);
                writeMovie(movie, fileWriter);
                writeRating(null, fileWriter); //write missing data
                writeClosingTags(fileWriter);
            }
        }
        return pair.getLeft();
    }

    private ImmutablePair<File, Boolean> createPathFile(String dirName, String fileName,
                                                        int suffix, String extension)
            throws IOException {
        File dir = new File(dirName);
        dir.mkdirs();
        String file = fileName + suffix + "." + extension;
        File outFile = new File(dir, file);
        boolean isNewFile = outFile.createNewFile();
        System.out.println((isNewFile ? "NEW: " : "OLD: ") + outFile.toPath());
        return new ImmutablePair<>(outFile, isNewFile);
    }


    private void writeHeader(boolean isTest, Writer fileWriter) throws IOException {
        fileWriter.write("<?xml version=\"1.0\" encoding=\"utf-8\"?>\n" +
                " \n" +
                " <!DOCTYPE dataset\n" +
                " [\n" +
                "    <!ELEMENT dataset (header,body)>\n" +
                "    <!ATTLIST dataset name CDATA #REQUIRED>\n" +
                "    <!ATTLIST dataset version CDATA \"3.5.4\">\n" +
                " \n" +
                "    <!ELEMENT header (notes?,attributes)>\n" +
                "    <!ELEMENT body (instances)>\n" +
                "    <!ELEMENT notes ANY>   <!--  comments, information, copyright, etc. -->\n" +
                " \n" +
                "    <!ELEMENT attributes (attribute+)>\n" +
                "    <!ELEMENT attribute (labels?,metadata?,attributes?)>\n" +
                "    <!ATTLIST attribute name CDATA #REQUIRED>\n" +
                "    <!ATTLIST attribute type (numeric|date|nominal|string|relational) #REQUIRED>\n" +
                "    <!ATTLIST attribute format CDATA #IMPLIED>\n" +
                "    <!ATTLIST attribute class (yes|no) \"no\">\n" +
                "    <!ELEMENT labels (label*)>   <!-- only for type \"nominal\" -->\n" +
                "    <!ELEMENT label ANY>\n" +
                "    <!ELEMENT metadata (property*)>\n" +
                "    <!ELEMENT property ANY>\n" +
                "    <!ATTLIST property name CDATA #REQUIRED>\n" +
                " \n" +
                "    <!ELEMENT instances (instance*)>\n" +
                "    <!ELEMENT instance (value*)>\n" +
                "    <!ATTLIST instance type (normal|sparse) \"normal\">\n" +
                "    <!ATTLIST instance weight CDATA #IMPLIED>\n" +
                "    <!ELEMENT value (#PCDATA|instances)*>\n" +
                "    <!ATTLIST value index CDATA #IMPLIED>   <!-- 1-based index (only used for instance format \"sparse\") -->\n" +
                "    <!ATTLIST value missing (yes|no) \"no\">\n" +
                " ]\n" +
                " >\n");
        fileWriter.write(String.format(" <dataset name=\"%s\" version=\"3.5.3\">\n", "movies_classification" +
                (isTest ? "-TEST" : "")));
        fileWriter.write("<header>\n");
        fileWriter.write("<attributes>\n");
        fileWriter.write("<attribute name=\"title\" type=\"string\"/>\n");
        fileWriter.write("<attribute name=\"genres\" type=\"string\"/>\n");
        fileWriter.write("<attribute name=\"actors\" type=\"string\"/>\n");
        fileWriter.write("<attribute name=\"keywords\" type=\"string\"/>\n");
        fileWriter.write("<attribute class=\"yes\" name=\"class\" type=\"nominal\">\n");
        fileWriter.write("<labels> \n" +
                "<label>1</label>\n" +
                "<label>2</label>\n" +
                "<label>3</label>\n" +
                "<label>4</label>\n" +
                "<label>5</label>\n" +
                "</labels>\n");
        fileWriter.write("</attribute>\n");
        fileWriter.write("</attributes>\n");
        fileWriter.write("</header>\n");

        fileWriter.write("<body>\n");
        fileWriter.write("<instances>\n");
    }

    private void writeRatedMovies(List<Rating> ratings, Writer fileWriter) throws IOException {
        for (Rating rating : ratings) {
            Movie m = rating.getMovie();
            movieService.initialize(m, EnumSet.allOf(Movie.LAZY.class));
            writeMovie(m, fileWriter);
            writeRating(rating.getStars(), fileWriter);
        }
        writeClosingTags(fileWriter);
    }

    private void writeMovie(Movie movie, Writer fileWriter) throws IOException {
        fileWriter.write("<instance>\n");
        fileWriter.write("<value>" + movie.getTitle() + "</value>\n");
        String genres = movie.getGenres().stream().map(Genre::getGenre).collect(Collectors.joining(";"));
        String actors = movie.getActors().stream().map(Actor::getName).collect(Collectors.joining(";"));
        String keywords = movie.getKeywords().stream().map(Keyword::getWord).collect(Collectors.joining(";"));
        fileWriter.write("<value>" + genres + "</value>\n");
        fileWriter.write("<value>" + actors + "</value>\n");
        fileWriter.write("<value>" + keywords + "</value>\n");
    }

    private void writeRating(Integer rating, Writer fileWriter) throws IOException {
        if (rating != null) {
            fileWriter.write("<value>" + rating + "</value>\n");
        } else
            fileWriter.write("<value>?</value>\n");
        fileWriter.write("</instance>\n");
    }

    private void writeClosingTags(Writer fileWriter) throws IOException {
        fileWriter.write("</instances>\n" +
                "    </body>\n" +
                " </dataset>\n");
    }


}
