package edu.kulikov.models;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIdentityReference;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "users_correlation")
@NamedQueries({
        @NamedQuery(name = UserCorrelation.FIND_BY_TWO_USERS, query = "SELECT uc FROM UserCorrelation uc WHERE " +
                "uc.user1=:user1 AND uc.user2=:user2 or uc.user1=:user2 AND uc.user2=:user1"),
        @NamedQuery(name = UserCorrelation.FIND_ALL_BY_USER, query = "SELECT uc FROM UserCorrelation uc WHERE " +
                "uc.user1=:user or uc.user2=:user"),
        //TODO: don't user weighted correlations!
        @NamedQuery(name = UserCorrelation.GET_TOP_N_BY_USER, query = "SELECT uc FROM UserCorrelation uc WHERE " +
                "uc.user1=:user or uc.user2=:user ORDER BY correlation * hybridWeight DESC"),
        @NamedQuery(name = UserCorrelation.SET_OUTDATED_FOR_USER, query = "UPDATE UserCorrelation AS uc SET " +
                "uc.isUpdated=false WHERE uc.user1=:user or uc.user2=:user"),
        @NamedQuery(name = UserCorrelation.GET_OUTDATED_FOR_USER, query = "SELECT uc FROM UserCorrelation uc WHERE " +
                "uc.user1=:user or uc.user2=:user and uc.isUpdated = false")})
public class UserCorrelation implements Serializable {

    public static final String FIND_BY_TWO_USERS = "UserCorrelation.findByTwoUsers";
    public static final String FIND_ALL_BY_USER = "UserCorrelation.findAllByUser";
    public static final String GET_TOP_N_BY_USER = "UserCorrelation.getTopNByUser";
    public static final String SET_OUTDATED_FOR_USER = "UserCorrelation.setOutdatedForUser";
    public static final String GET_OUTDATED_FOR_USER = "UserCorrelation.getOutdatedForUser";

    @Id
    @ManyToOne
    @JoinColumn(name = "userID1", referencedColumnName = "id")
    @JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id")
    @OnDelete(action = OnDeleteAction.CASCADE)
    @JsonIdentityReference(alwaysAsId = true)
    private User user1;
    @Id
    @ManyToOne
    @OnDelete(action = OnDeleteAction.CASCADE)
    @JoinColumn(name = "userID2", referencedColumnName = "id")
    @JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id")
    @JsonIdentityReference(alwaysAsId = true)
    private User user2;
    private double correlation;
    @Column(name = "is_updated")
    private boolean isUpdated;
    @Column(name = "hybrid_weight")
    private double hybridWeight;

    public UserCorrelation() {
    }

    public UserCorrelation(User user1, User user2, double correlation) {
        this.user1 = user1;
        this.user2 = user2;
        this.correlation = correlation;
    }

    public User getUser1() {
        return user1;
    }

    public void setUser1(User user1) {
        this.user1 = user1;
    }

    public User getUser2() {
        return user2;
    }

    public void setUser2(User user2) {
        this.user2 = user2;
    }

    public double getCorrelation() {
        return correlation;
    }

    public void setCorrelation(double correlation) {
        this.correlation = correlation;
    }

    public boolean isUpdated() {
        return isUpdated;
    }

    public void setUpdated(boolean isUpdated) {
        this.isUpdated = isUpdated;
    }

    public double getHybridWeight() {
        return hybridWeight;
    }

    public void setHybridWeight(double hybridWeight) {
        this.hybridWeight = hybridWeight;
    }

    /**
     * Sort users' ID such that userID1 < userID2
     */
    public void orderUserIDs() {
        if (user2.getId() < user1.getId()) {
            User temp = user1;
            user1 = user2;
            user2 = temp;
        }
    }

    public UserCorrelation putToFirst(Integer userId) {
        if (user2.getId() == userId) {
            User temp = user1;
            user1 = user2;
            user2 = temp;
        }
        return this;
    }


}
