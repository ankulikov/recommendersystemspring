package edu.kulikov.models;


import javax.persistence.AttributeConverter;

public class StringArrayConverter implements AttributeConverter<String[], String> {
    @Override
    public String convertToDatabaseColumn(String[] attribute) {
        return String.join(", ", attribute);
    }

    @Override
    public String[] convertToEntityAttribute(String dbData) {
        return dbData.split(", ");
    }
}
