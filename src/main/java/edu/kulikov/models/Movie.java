package edu.kulikov.models;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "movies")
@NamedQueries({
        @NamedQuery(name = Movie.FIND_BY_ID, query = "SELECT m FROM Movie m WHERE m.id=:id"),
        @NamedQuery(name = Movie.GET_ALL, query = "SELECT m FROM Movie m"),
        @NamedQuery(name = Movie.GET_COUNT, query = "SELECT COUNT(m) FROM Movie m"),
        @NamedQuery(name = Movie.GET_UNRATED, query = "SELECT m FROM Movie m WHERE m NOT IN " +
                "(SELECT r.movie FROM Rating r WHERE r.user=:user)")
})
public class Movie implements java.io.Serializable {

    public static final String FIND_BY_ID = "Movie.findById";
    public static final String GET_ALL = "Movie.getAll";
    public static final String GET_COUNT = "Movie.getCount";
    public static final String GET_UNRATED = "Movies.getUnratedMovies";


    public enum LAZY {
        ACTORS, GENRES, KEYWORDS, RATINGS
    }


    public Movie() {
    }

    @Id
    @GeneratedValue
    private int id;
    @Column(nullable = false)
    private String title;
    private int year;

    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(name = "movies_actors", joinColumns = {@JoinColumn(name = "movie_id")},
            inverseJoinColumns = {@JoinColumn(name = "actor_id")})
    private List<Actor> actors;

    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(name = "movies_genres", joinColumns = {@JoinColumn(name = "movie_id")},
            inverseJoinColumns = {@JoinColumn(name = "genre_id")})
    private List<Genre> genres;

    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(name = "movies_keywords", joinColumns = {@JoinColumn(name = "movie_id")},
            inverseJoinColumns = {@JoinColumn(name = "keyword_id")})
    private List<Keyword> keywords;

    @Column(columnDefinition = "text")
    private String plot;

    @Column(name = "poster_url")
    private String posterUrl;

    @Column(name = "imdb_id")
    private String imdbId;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "movie")
    private List<Rating> ratings;


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public List<Actor> getActors() {
        return actors;
    }

    public void setActors(List<Actor> actors) {
        this.actors = actors;
    }

    public List<Genre> getGenres() {
        return genres;
    }

    public void setGenres(List<Genre> genres) {
        this.genres = genres;
    }

    public List<Keyword> getKeywords() {
        return keywords;
    }

    public void setKeywords(List<Keyword> keywords) {
        this.keywords = keywords;
    }

    public String getPlot() {
        return plot;
    }

    public void setPlot(String plot) {
        this.plot = plot;
    }

    public String getPosterUrl() {
        return posterUrl;
    }

    public void setPosterUrl(String posterUrl) {
        this.posterUrl = posterUrl;
    }

    public String getImdbId() {
        return imdbId;
    }

    public void setImdbId(String imdbId) {
        this.imdbId = imdbId;
    }

    public List<Rating> getRatings() {
        return ratings;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Movie)) return false;

        Movie movie = (Movie) o;

        return id == movie.id;

    }

    @Override
    public int hashCode() {
        return id;
    }
}
