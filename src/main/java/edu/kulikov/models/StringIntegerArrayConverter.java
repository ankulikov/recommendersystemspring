package edu.kulikov.models;

import javax.persistence.AttributeConverter;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class StringIntegerArrayConverter implements AttributeConverter<List, String> {
    @Override
    public String convertToDatabaseColumn(List attribute) {
        //noinspection unchecked
        return ((List<Integer>)attribute)
                .stream()
                .map(String::valueOf)
                .collect(Collectors.joining(";","",""));
    }

    @Override
    public List<Integer> convertToEntityAttribute(String dbData) {
        return Arrays.asList(dbData.split(";"))
                .stream()
                .map(Integer::parseInt)
                .collect(Collectors.toList());
    }
}
