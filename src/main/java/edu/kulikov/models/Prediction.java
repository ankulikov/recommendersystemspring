package edu.kulikov.models;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIdentityReference;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import javax.persistence.*;
import java.io.Serializable;


@Entity
@Table(name = "predictions")
@NamedQueries({
        @NamedQuery(name = Prediction.AVERAGE_CONTENT_PREDICTION_FOR_USER,
                query = "SELECT AVG(p.contentPrediction) FROM Prediction p WHERE user=:user"),
        @NamedQuery(name = Prediction.PREDICTION_USER_MOVIE,
                query = "SELECT p FROM Prediction p WHERE user=:user and movie=:movie"),
        @NamedQuery(name = Prediction.TOP_N_PREDICTIONS_FOR_USER,
                query = "SELECT p FROM Prediction p WHERE user=:user ORDER BY boostedPrediction DESC"),
        @NamedQuery(name = Prediction.CONTENT_PREDICTION_SET_OUTDATED_FOR_USER,
                query = "UPDATE Prediction AS p SET isContentUpdated = false " +
                        "WHERE p.user=:user"),
        @NamedQuery(name = Prediction.GET_UPDATED_PREDICTIONS_FOR_USER,
                query = "SELECT p FROM Prediction p WHERE isContentUpdated = true and user=:user")
})
public class Prediction implements Serializable {

    public static final String AVERAGE_CONTENT_PREDICTION_FOR_USER =
            "Prediction.getAverageContentPredictionForUser";
    public static final String PREDICTION_USER_MOVIE =
            "Prediction.getPredictionUserMovie";
    public static final String CONTENT_PREDICTION_SET_OUTDATED_FOR_USER =
            "Prediction.setContentPredictionOutdated";
    public static final String GET_UPDATED_PREDICTIONS_FOR_USER = "Prediction.getUpdatedPredictions";
    public static final String TOP_N_PREDICTIONS_FOR_USER = "Prediction.topNPredictionsForUser";


    @Id
    @ManyToOne
    @OnDelete(action = OnDeleteAction.CASCADE)
    @JoinColumn(name = "user", referencedColumnName = "id")
    @JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id")
    @JsonIdentityReference(alwaysAsId = true)
    private User user;

    @Id
    @ManyToOne
    @JoinColumn(name = "movie", referencedColumnName = "id")
    @JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id")
    @JsonIdentityReference(alwaysAsId = true)
    private Movie movie;

    @Column(name = "content_prediction")
    private Integer contentPrediction;
    @Column(name = "collaborative_prediction")
    private Integer collaborativePrediction;
    @Column(name = "boosted_prediction")
    private Double boostedPrediction;
    @Column(name = "is_content_updated", nullable = false)
    private boolean isContentUpdated;

    public Prediction() {

    }

    public Prediction(User user, Movie movie) {
        this.user = user;
        this.movie = movie;
    }

    public User getUser() {
        return user;
    }

    public Movie getMovie() {
        return movie;
    }

    public Integer getContentPrediction() {
        return contentPrediction;
    }

    public Integer getCollaborativePrediction() {
        return collaborativePrediction;
    }

    public Double getBoostedPrediction() {
        return boostedPrediction;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public void setMovie(Movie movie) {
        this.movie = movie;
    }

    public void setContentPrediction(Integer contentPrediction) {
        this.contentPrediction = contentPrediction;
    }

    public void setCollaborativePrediction(Integer collaborativePrediction) {
        this.collaborativePrediction = collaborativePrediction;
    }

    public void setBoostedPrediction(Double boostedPrediction) {
        this.boostedPrediction = boostedPrediction;
    }

    public boolean isContentUpdated() {
        return isContentUpdated;
    }

    public void setContentUpdated(boolean isContentUpdated) {
        this.isContentUpdated = isContentUpdated;
    }
}
