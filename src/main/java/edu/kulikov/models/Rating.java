package edu.kulikov.models;


import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIdentityReference;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "ratings")
@NamedQueries({
        @NamedQuery(name = Rating.AVERAGE_RATED_FOR_USER,
                query = "SELECT AVG(r.stars) FROM Rating r WHERE user=:user"),
        @NamedQuery(name = Rating.RATED_COUNT_FOR_USER,
                query = "SELECT COUNT(*) FROM Rating r WHERE user=:user AND stars IS NOT NULL"),
        @NamedQuery(name = Rating.GET_ALL_RATED_FOR_USER,
                query = "SELECT r FROM Rating r WHERE user=:user AND stars IS NOT NULL"),
        @NamedQuery(name = Rating.GET_RATING_USER_MOVIE,
                query = "SELECT r FROM Rating r WHERE user=:user AND movie=:movie AND stars IS NOT NULL"),
        @NamedQuery(name = Rating.GET_COUNT,
        query = "SELECT COUNT(*) FROM Rating r")

})

public class Rating implements Serializable {

    public static final String AVERAGE_RATED_FOR_USER = "Rating.getAverageRatedForUser";
    public static final String GET_ALL_RATED_FOR_USER = "Rating.getAllRatedForUser";
    public static final String RATED_COUNT_FOR_USER = "Rating.getRatedCountForUser";
    public static final String GET_RATING_USER_MOVIE = "Rating.getRating";
    public static final String GET_COUNT = "Rating.getCount";

    @Id
    @ManyToOne
    @OnDelete(action = OnDeleteAction.CASCADE)
    @JoinColumn(name = "user", referencedColumnName = "id")
    @JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id")
    @JsonIdentityReference(alwaysAsId = true)
    private User user;

    @Id
    @ManyToOne
    @OnDelete(action = OnDeleteAction.CASCADE)
    @JoinColumn(name = "movie", referencedColumnName = "id")
    @JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id")
    @JsonIdentityReference(alwaysAsId = true)
    private Movie movie;

    @Column(columnDefinition = "tinyint(3)")
    private Integer stars;


    public Rating() {
    }

    public Rating(User user, Movie movie, int stars) {
        this.user = user;
        this.movie = movie;
        this.stars = stars;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Movie getMovie() {
        return movie;
    }

    public void setMovie(Movie movie) {
        this.movie = movie;
    }

    public Integer getStars() {
        return stars;
    }

    public void setStars(Integer stars) {
        this.stars = stars;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Rating rating = (Rating) o;

        return !(movie != null ? !movie.equals(rating.movie) : rating.movie != null);
    }

    @Override
    public int hashCode() {
        return movie != null ? movie.hashCode() : 0;
    }
}

