package edu.kulikov.models;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

@Entity
@Table(name = "users")
@NamedQueries({
        @NamedQuery(name = User.FIND_BY_ID, query = "SELECT u FROM User u WHERE u.id=:id"),
        @NamedQuery(name = User.GET_ALL, query = "SELECT u FROM User u"),
        @NamedQuery(name = User.GET_COUNT, query = "SELECT COUNT(u) FROM User u"),
        @NamedQuery(name = User.GET_ALL_WITH_OUTDATED_CONTENT_PREDICTIONS,
        query = "SELECT u FROM User u WHERE contentPredictionUpdated = false")
})

public class User implements Serializable {

    public static final String FIND_BY_ID = "User.findById";
    public static final String GET_ALL = "User.getAll";
    public static final String GET_COUNT = "User.getCount";
    public static final String GET_ALL_WITH_OUTDATED_CONTENT_PREDICTIONS =
            "User.getAllWithOutdatedContentPredictions";

    @Id
    @GeneratedValue
    private int id;
    @Column(nullable = false)
    private int age;
    @Column(nullable = false)
    private boolean isMale;
    @Column(nullable = false)
    private String occupation;
    @Column(name = "content_prediction_updated", nullable = false)
    private boolean contentPredictionUpdated;
    @Column(name="recommended", nullable = false)
    private boolean recommended;
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "user")
    private List<Rating> ratings;




    public enum LAZY {
        RATINGS
    }

    public User() {
    }

    public User(int age, boolean isMale, String occupation) {
        this.age = age;
        this.isMale = isMale;
        this.occupation = occupation;
    }

    public int getId() {
        return id;
    }

    public int getAge() {
        return age;
    }

    public boolean isMale() {
        return isMale;
    }

    public String getOccupation() {
        return occupation;
    }

    public List<Rating> getRatings() {
        return ratings;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public void setMale(boolean isMale) {
        this.isMale = isMale;
    }

    public void setOccupation(String occupation) {
        this.occupation = occupation;
    }

    public void setRatings(List<Rating> ratings) {
        this.ratings = ratings;
    }

    public boolean isContentPredictionUpdated() {
        return contentPredictionUpdated;
    }

    public void setContentPredictionUpdated(boolean contentPredictionUpdated) {
        this.contentPredictionUpdated = contentPredictionUpdated;
    }

    public boolean isRecommended() {
        return recommended;
    }

    public void setRecommended(boolean recommended) {
        this.recommended = recommended;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof User)) return false;

        User user = (User) o;

        return id == user.id;

    }

    @Override
    public int hashCode() {
        return id;
    }

    @Override
    public String toString() {
        return "User{" +
                "id=" + id +
                ", age=" + age +
                ", isMale=" + isMale +
                ", occupation='" + occupation + '\'' +
                '}';
    }
}
