package edu.kulikov.models;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIdentityReference;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;
import org.hibernate.annotations.Type;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

@Entity
@Table(name = "metrics")
@NamedQueries({
        @NamedQuery(name=Metric.FIND_BY_USER, query = "SELECT m FROM Metric m where user=:user"),
        @NamedQuery(name = Metric.GET_ALL, query = "SELECT m FROM Metric m")
})
public class Metric implements Serializable {
    public static final String FIND_BY_USER = "Metric.findByUser";
    public static final String GET_ALL = "Metric.getAll";
    @Id
    @GeneratedValue
    private Integer id;

    @ManyToOne
    @OnDelete(action = OnDeleteAction.CASCADE)
    @JoinColumn(name = "user", referencedColumnName = "id")
    @JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id")
    @JsonIdentityReference(alwaysAsId = true)
    private User user;

    @Column(name = "test_set_count")
    private Integer testSetCount;
    @Column(name = "train_set_count")
    private Integer trainSetCount;
    @Column(name = "tpr", columnDefinition = "TEXT")
    @Convert(converter = StringDoubleArrayConverter.class)
    public List<Double> tpr;
    @Column(name="fpr", columnDefinition = "TEXT")
    @Convert(converter = StringDoubleArrayConverter.class)
    public List<Double> fpr;
    @Column(name = "threshold", columnDefinition = "TEXT")
    @Convert(converter = StringDoubleArrayConverter.class)
    public List<Double> threshold;

    public Metric() {

    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Integer getTestSetCount() {
        return testSetCount;
    }

    public void setTestSetCount(Integer testSetCount) {
        this.testSetCount = testSetCount;
    }

    public Integer getTrainSetCount() {
        return trainSetCount;
    }

    public void setTrainSetCount(Integer trainSetCount) {
        this.trainSetCount = trainSetCount;
    }

    public List<Double> getTpr() {
        return tpr;
    }

    public void setTpr(List<Double> tpr) {
        this.tpr = tpr;
    }

    public List<Double> getFpr() {
        return fpr;
    }

    public void setFpr(List<Double> fpr) {
        this.fpr = fpr;
    }

    public List<Double> getThreshold() {
        return threshold;
    }

    public void setThreshold(List<Double> threshold) {
        this.threshold = threshold;
    }
}
