package edu.kulikov.models;

import javax.persistence.AttributeConverter;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class StringDoubleArrayConverter implements AttributeConverter<List, String> {
    @Override
    public String convertToDatabaseColumn(List attribute) {
        //noinspection unchecked
        String collect = ((List<Double>) attribute)
                .stream()
                .map(String::valueOf)
                .collect(Collectors.joining(";", "", ""));
        System.out.println(collect);
        return collect;
    }

    @Override
    public List<Double> convertToEntityAttribute(String dbData) {
        return Arrays.asList(dbData.split(";"))
                .stream()
                .map(Double::parseDouble)
                .collect(Collectors.toList());
    }
}
