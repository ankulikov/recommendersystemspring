package edu.kulikov.models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import javax.persistence.*;

@SuppressWarnings("serial")
@Entity
@Table(name = "account")
@NamedQuery(name = Account.FIND_BY_LOGIN, query = "select a from Account a where a.login = :login")
public class Account implements java.io.Serializable {

	public static final String FIND_BY_LOGIN = "Account.findByLogin";

	public enum LAZY {
		USER
	}

	@Id
	@GeneratedValue
	private Long id;

	@Column(unique = true)
	private String login;

	@OneToOne(fetch = FetchType.LAZY)
	@OnDelete(action = OnDeleteAction.CASCADE)
	private User user;

	@JsonIgnore
	private String password;

	private String role = "ROLE_USER";

    Account() {

	}

	public Account(String login, String password, String role) {
		this.login = login;
		this.password = password;
		this.role = role;
	}

	public Long getId() {
		return id;
	}

    public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getRole() {
		return role;
	}

	public void setRole(String role) {
		this.role = role;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}
}
