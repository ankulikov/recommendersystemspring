package edu.kulikov;

/**
 * Marker interface to detect app root package
 */
public interface Application {}
