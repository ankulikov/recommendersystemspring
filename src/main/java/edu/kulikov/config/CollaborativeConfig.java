package edu.kulikov.config;

import edu.kulikov.collaborative.CollaborativeParams;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;

@Configuration
public class CollaborativeConfig {
    @Value("${collaborative.neighbours}")
    private int neighbours;
    @Value("${collaborative.min_overlapping_items}")
    private int minOverlappingItems;
    @Value("${collaborative.harmonic_threshold}")
    private int harmonicThreshold;
    @Value("${collaborative.self_weighting_threshold}")
    private int selfWeightingThreshold;
    @Value("${collaborative.max_self_weighting}")
    private int maxSelfWeighting;

    private CollaborativeParams params;


    @Bean
    public CollaborativeParams collaborativeParamsCreator() {
        if (params == null)
            params = new CollaborativeParams(neighbours, minOverlappingItems, harmonicThreshold, selfWeightingThreshold, maxSelfWeighting);
        return params;
    }

    @Bean
    public static PropertySourcesPlaceholderConfigurer placeHolderConfigurer() {
        return new PropertySourcesPlaceholderConfigurer();
    }






}
