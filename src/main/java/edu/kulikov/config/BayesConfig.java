package edu.kulikov.config;

import edu.kulikov.bayes.BayesDataCreator;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;

@Configuration
public class BayesConfig {
    @Value("${bayes.train_dir}")
    private String trainDirName;
    @Value("${bayes.train_file}")
    private String trainFileName;
    @Value("${bayes.test_dir}")
    private String testDirName;
    @Value("${bayes.test_file}")
    private String testFileName;


    private BayesDataCreator bayesDataCreator;

    @Bean
    public BayesDataCreator bayesDataCreator() {
        if (bayesDataCreator == null)
            bayesDataCreator = new BayesDataCreator(trainDirName, trainFileName, testDirName, testFileName);
        return bayesDataCreator;
    }

    @Bean
    public static PropertySourcesPlaceholderConfigurer placeHolderConfigurer() {
        return new PropertySourcesPlaceholderConfigurer();
    }

}
