package edu.kulikov.dao;

import edu.kulikov.models.Metric;
import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;

@Repository
public class MetricRepository {
    @PersistenceContext
    private EntityManager entityManager;

    public List<Pair<Integer, Integer>> getRealAndPredicted(Integer origUsedId, Integer testUserId) {
        String query = "select stars, CAST(boosted_prediction AS unsigned) " +
                "from predictions p, ratings r " +
                "where r.user = ? and p.user = ? and r.movie = p.movie";
        //noinspection unchecked
        List<Object[]> results = (List<Object[]>) entityManager.createNativeQuery(query)
                .setParameter(1, origUsedId)
                .setParameter(2, testUserId)
                .getResultList();
        List<Pair<Integer, Integer>> toReturn = new ArrayList<>();
        for (Object[] result : results) {
            toReturn.add(new ImmutablePair<>(
                    ((Byte) result[0]).intValue(),
                    ((BigInteger) result[1]).intValue()));
        }
        return toReturn;
    }

    public Metric getById(Integer id) {
        return entityManager.find(Metric.class, id);
    }

    public List<Metric> getAll() {
        return entityManager.createNamedQuery(Metric.GET_ALL, Metric.class)
                .getResultList();
    }

    @Transactional
    public Metric save(Metric metric) {
        if (metric.getId()== null || getById(metric.getId())==null) {
            entityManager.persist(metric);
            return metric;
        } else
            return entityManager.merge(metric);
    }


}
