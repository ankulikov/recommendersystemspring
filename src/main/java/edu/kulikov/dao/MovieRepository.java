package edu.kulikov.dao;

import edu.kulikov.models.Movie;
import edu.kulikov.models.User;
import org.hibernate.Hibernate;
import org.hibernate.Session;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceException;
import java.util.EnumSet;
import java.util.List;

@Repository
public class MovieRepository {

    @PersistenceContext
    private EntityManager entityManager;

    @Transactional
    public Movie getById(int id, EnumSet<Movie.LAZY> fields) {
        try {
            Movie movie = entityManager.createNamedQuery(Movie.FIND_BY_ID, Movie.class)
                    .setParameter("id", id)
                    .getSingleResult();
            if (movie!= null && fields != null && !fields.isEmpty()) {
                initialize(movie, fields);
            }
            return movie;
        } catch (PersistenceException e) {
            return null;
        }
    }

    @Transactional
    public List<Movie> getAll(int count, int startIndex, EnumSet<Movie.LAZY> fields) {
        try {
            List<Movie> movies = entityManager.createNamedQuery(Movie.GET_ALL, Movie.class)
                    .setMaxResults(count)
                    .setFirstResult(startIndex)
                    .getResultList();
            if (fields != null && !fields.isEmpty()) {
                for (Movie movie : movies) {
                    initialize(movie, fields);
                }
            }
            return movies;
        } catch (PersistenceException e) {
            return null;
        }
    }

    @Transactional
    public List<Movie> getAll(EnumSet<Movie.LAZY> fields) {
        try {
            List<Movie> movies = entityManager.createNamedQuery(Movie.GET_ALL, Movie.class).getResultList();
            if (fields != null && !fields.isEmpty()) {
                for (Movie movie : movies) {
                    initialize(movie, fields);
                }
            }
            return movies;
        } catch (PersistenceException e) {
            return null;
        }
    }

    @Transactional
    public List<Movie> getUnratedMovies(User user) {
        try {
            List<Movie> movies = entityManager.createNamedQuery(Movie.GET_UNRATED, Movie.class)
                    .setParameter("user", user)
                    .getResultList();
            return movies;
        } catch (PersistenceException e) {
            return null;
        }

    }


    public Long getCount() {
        try {
            return entityManager.createNamedQuery(Movie.GET_COUNT, Long.class).getSingleResult();
        } catch (PersistenceException e) {
            return null;
        }

    }

    @Transactional
    public void initialize(Movie movie, EnumSet<Movie.LAZY> fields) {
        Session session = entityManager.unwrap(Session.class);
        session.refresh(movie);
        for (Movie.LAZY field : fields) {
            switch (field) {
                case ACTORS:
                    Hibernate.initialize(movie.getActors());
                    break;
                case GENRES:
                    Hibernate.initialize(movie.getGenres());
                    break;
                case KEYWORDS:
                    Hibernate.initialize(movie.getKeywords());
                    break;
                case RATINGS:
                    Hibernate.initialize(movie.getRatings());
                    break;
            }
        }
    }


}
