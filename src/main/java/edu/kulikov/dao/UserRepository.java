package edu.kulikov.dao;

import edu.kulikov.models.Account;
import edu.kulikov.models.User;
import edu.kulikov.services.AccountService;
import org.hibernate.Hibernate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceException;
import java.util.EnumSet;
import java.util.List;

@Repository
public class UserRepository {
    @PersistenceContext
    private EntityManager entityManager;
    @Autowired
    private AccountService accountService;

    /**
     * Loads only profile information without ratings
     *
     * @param id     user id
     * @param fields fields which must be initialized for the user
     * @return user object populated only with profile information
     */
    @Transactional
    public User getById(int id, EnumSet<User.LAZY> fields) {
        try {
            User user = entityManager.createNamedQuery(User.FIND_BY_ID, User.class)
                    .setParameter("id", id)
                    .getSingleResult();
            initialize(user, fields);
            return user;
        } catch (PersistenceException e) {
            return null;
        }
    }

    @Transactional
    public User getByLogin(String login, EnumSet<User.LAZY> fields) {
        Account account = accountService.getByLogin(login);
        if (account != null) {
            accountService.initialize(account, EnumSet.of(Account.LAZY.USER));
            User user = account.getUser();
            if (user != null)
                initialize(user, fields);
            return user;
        }
        return null;
    }

    @Transactional
    public List<User> getAll(int count, int startIndex, EnumSet<User.LAZY> fields) {
        try {
            List<User> users = entityManager.createNamedQuery(User.GET_ALL, User.class)
                    .setMaxResults(count)
                    .setFirstResult(startIndex)
                    .getResultList();
            if (fields != null && !fields.isEmpty()) {
                for (User user : users) {
                    initialize(user, fields);
                }
            }
            return users;

        } catch (PersistenceException e) {
            return null;
        }
    }

    @Transactional
    public List<User> getAll(EnumSet<User.LAZY> fields) {
        try {
            List<User> users = entityManager.createNamedQuery(User.GET_ALL, User.class)
                    .getResultList();
            if (fields != null && !fields.isEmpty()) {
                for (User user : users) {
                    initialize(user, fields);
                }
            }
            return users;

        } catch (PersistenceException e) {
            return null;
        }
    }

    public List<User> getAllWithOutdatedContentPredictions() {
        try {
            return entityManager.createNamedQuery(
                    User.GET_ALL_WITH_OUTDATED_CONTENT_PREDICTIONS, User.class)
                    .getResultList();
        } catch (PersistenceException e) {
            return null;
        }
    }


    public Long getCount() {
        try {
            return entityManager.createNamedQuery(User.GET_COUNT, Long.class).getSingleResult();
        } catch (PersistenceException e) {
            e.printStackTrace();
            return null;
        }
    }

    private User initialize(User user, EnumSet<User.LAZY> fields) {
        if (fields != null && !fields.isEmpty()) {
            for (User.LAZY field : fields) {
                switch (field) {
                    case RATINGS:
                        Hibernate.initialize(user.getRatings());
                        break;
                    default:
                        throw new IllegalArgumentException("Unknown field!");
                }
            }
        }
        return user;
    }


    @Transactional
    public User save(User user) {
        if (entityManager.find(User.class, user.getId()) == null) {
            entityManager.persist(user);
            return user;
        } else {
            return entityManager.merge(user);
        }
    }

    @Transactional
    public void delete(User user) {
        if (user != null && getById(user.getId(), null) != null) {
            entityManager.remove(entityManager.merge(user));
        }
    }


}
