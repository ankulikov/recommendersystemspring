package edu.kulikov.dao;

import edu.kulikov.models.Movie;
import edu.kulikov.models.Prediction;
import edu.kulikov.models.Rating;
import edu.kulikov.models.User;
import edu.kulikov.services.RatingService;
import edu.kulikov.services.UserCorrelationService;
import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;
import org.hibernate.Session;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceException;
import javax.persistence.Query;
import java.math.BigDecimal;
import java.util.List;
import java.util.stream.Collectors;

@Repository
public class PredictionRepository {

    @PersistenceContext
    private EntityManager entityManager;

    @Autowired
    private UserCorrelationService correlationService;
    @Autowired
    private RatingService ratingService;

    public Double getAverageContentPrediction(User user) {
        try {
            return entityManager.createNamedQuery(Prediction.AVERAGE_CONTENT_PREDICTION_FOR_USER,
                    Double.class)
                    .setParameter("user", user)
                    .getSingleResult();
        } catch (PersistenceException ex) {
            ex.printStackTrace();
            return null;
        }
    }

    public Integer getPseudoStars(User user, Movie movie) {
        Rating rating = ratingService.getRating(user.getId(), movie.getId());
        if (rating == null || rating.getStars() == null) {
            Prediction prediction = getPredictionUserMovie(user, movie);
            if (prediction != null && prediction.getContentPrediction() != null)
                return prediction.getContentPrediction();
        } else {
            return rating.getStars();
        }
        return null;
    }

    public Double getAveragePseudoStarsForUser(User user) {
        try {
            return ((BigDecimal) entityManager.createNativeQuery("SELECT avg(result.stars) FROM " +
                    "((SELECT user, movie, stars from ratings where user = ?) " +
                    "union " +
                    "(SELECT user, movie, content_prediction as stars from predictions where user = ?)) result")
                    .setParameter(1, user.getId())
                    .setParameter(2, user.getId())
                    .getSingleResult()).doubleValue(); //BigDecimal?! wtf...
        } catch (PersistenceException ex) {
            ex.printStackTrace();
            return null;
        }
    }

    //returns null if no row in table with such user and movie
    public Prediction getPredictionUserMovie(User user, Movie movie) {
        try {
            return entityManager.createNamedQuery(Prediction.PREDICTION_USER_MOVIE,
                    Prediction.class)
                    .setParameter("user", user)
                    .setParameter("movie", movie)
                    .getSingleResult();
        } catch (PersistenceException ex) {
            // ex.printStackTrace();
            return null;
        }
    }

    @Transactional
    public void setContentPredictionOutdatedForUser(User user) {
        try {
            entityManager.createNamedQuery(Prediction.CONTENT_PREDICTION_SET_OUTDATED_FOR_USER)
                    .setParameter("user", user)
                    .executeUpdate();
        } catch (PersistenceException ex) {
            ex.printStackTrace();
        }
    }

    @Transactional
    public Prediction save(Prediction prediction) {
        if (entityManager.find(Prediction.class,
                new Prediction(prediction.getUser(), prediction.getMovie())) == null) {
            entityManager.persist(prediction);
            System.out.println("persist: movie: " + prediction.getMovie().getId());
            return prediction;
        } else {
            System.out.println("merge: movie: " + prediction.getMovie().getId());
            Prediction merged = entityManager.merge(prediction);
            return merged;
        }
    }


    @Transactional
    public void delete(Prediction prediction) {
        if (entityManager.find(Prediction.class, prediction) != null)
            entityManager.remove(entityManager.merge(prediction));
    }

    @Transactional
    public List<Prediction> getUpdatedPredictionsForUser(User user) {
        try {
            return entityManager.createNamedQuery(Prediction.GET_UPDATED_PREDICTIONS_FOR_USER,
                    Prediction.class)
                    .setParameter("user", user)
                    .getResultList();
        } catch (PersistenceException ex) {
            ex.printStackTrace();
            return null;
        }
    }

    @Transactional
    public List<Pair<Integer, Byte>> getAllPseudoStars(Integer userId) {
        Query query = entityManager.createNativeQuery("(SELECT movie, stars from ratings where user = ?) " +
                "union " +
                "(SELECT movie, content_prediction as stars from predictions where user = ?)");
        //noinspection unchecked
        List<Object[]> resultList = (List<Object[]>) query
                .setParameter(1, userId)
                .setParameter(2, userId)
                .getResultList();
        return resultList.stream().map(row -> new ImmutablePair<>(
                (Integer) row[0],
                ((Integer) row[1]).byteValue()
        )).collect(Collectors.toList());
    }

    public List<Prediction> getTopNPredictionsForUser(User user, int n) {
        try {
            return entityManager.createNamedQuery(Prediction.TOP_N_PREDICTIONS_FOR_USER,
                    Prediction.class)
                    .setParameter("user", user)
                    .setMaxResults(n)
                    .getResultList();
        } catch (PersistenceException ex) {
            ex.printStackTrace();
            return null;
        }
    }


}
