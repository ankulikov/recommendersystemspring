package edu.kulikov.dao;

import javax.persistence.*;
import javax.inject.Inject;

import edu.kulikov.models.Account;
import org.hibernate.Hibernate;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.security.crypto.password.PasswordEncoder;

import java.util.EnumSet;

@Repository
public class AccountRepository {
	
	@PersistenceContext
	private EntityManager entityManager;
	
	@Inject
	private PasswordEncoder passwordEncoder;
	
	@Transactional
	public Account save(Account account) {
		account.setPassword(passwordEncoder.encode(account.getPassword()));
		entityManager.persist(account);
		return account;
	}
	
	public Account findByLogin(String login) {
		try {

			return entityManager.createNamedQuery(Account.FIND_BY_LOGIN, Account.class)
					.setParameter("login", login)
					.getSingleResult();
		} catch (PersistenceException e) {
			return null;
		}
	}


	@Transactional
	public void initialize(Account account, EnumSet<Account.LAZY> fields) {
		for (Account.LAZY field : fields) {
			switch (field) {
				case USER:
					Hibernate.initialize(account.getUser());
					break;
				default:
					throw new IllegalArgumentException("Unknown field of account");
			}
		}
	}
}
