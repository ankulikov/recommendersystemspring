package edu.kulikov.dao;

import edu.kulikov.models.Movie;
import edu.kulikov.models.Rating;
import edu.kulikov.models.User;
import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.*;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Repository
public class RatingRepository {

    @PersistenceContext
    private EntityManager entityManager;

    private final static String COMMON_MOVIES = "SELECT r1.movie, r1.stars as stars1, r2.stars as stars2 " +
            " from ratings r1, ratings r2 where r1.user = ? and r2.user = ? and" +
            " r1.movie = r2.movie";
    private final static String COMMON_MOVIES_COUNT = "SELECT count(*) " +
            " from ratings r1, ratings r2 where r1.user = ? and r2.user = ? and" +
            " r1.movie = r2.movie";
    private final static String ALL_OCCUPATIONS_SHARE_BY_STARS = "select occupation, " +
            "CAST(count(*) / " +
            "(select count(*) from users u2, ratings r2 where u2.occupation = u1.occupation and r2.user = u2.id) " +
            "* 100 AS UNSIGNED) " +
            "from ratings r1, users u1 " +
            "where r1.user = u1.id and r1.stars = ? group by u1.occupation";
    private final static String ALL_AGES_SHARE_BY_STARS = "select " +
            "CONCAT(min(u1.age),' - ', max(u1.age)), " +
            "CAST(count(*) / " +
            " (select count(*) from users u2, ratings r2 " +
            "where r2.user = u2.id and " +
            "CAST(TRUNCATE(u1.age / 10, 0) as UNSIGNED) = CAST(TRUNCATE(u2.age / 10, 0) as UNSIGNED) " +
            ") *100 as unsigned) " +
            "from ratings r1, users u1 " +
            "where r1.user = u1.id and u1.age > 9 and r1.stars = ? " +
            "group by CAST(TRUNCATE(u1.age / 10, 0) as UNSIGNED)";
    private final static String ALL_GENDERS_SHARE_BY_STARS = "select isMale, " +
            "CAST(count(*) / "+
            "(select count(*) from users u2, ratings r2 where u2.isMale = u1.isMale and r2.user = u2.id) "+
            "* 100 AS UNSIGNED) "+
            "from ratings r1, users u1 "+
            "where r1.user = u1.id and r1.stars = ? group by u1.isMale";

    public Pair<List<Rating>, List<Rating>> getCommonMoviesRatings(User user1, User user2) {

        Query nativeQuery = entityManager.createNativeQuery(COMMON_MOVIES);
        nativeQuery.setParameter(1, user1.getId());
        nativeQuery.setParameter(2, user2.getId());

        @SuppressWarnings("unchecked") List<Object[]> resultList = nativeQuery.getResultList();
        List<Rating> rs1 = new ArrayList<>();
        List<Rating> rs2 = new ArrayList<>();
        for (Object[] commonMoviesRating : resultList) {
            Movie movie = new Movie();
            movie.setId((Integer) commonMoviesRating[0]);
            rs1.add(new Rating(user1, movie, (Byte) commonMoviesRating[1]));
            rs2.add(new Rating(user2, movie, (Byte) commonMoviesRating[2]));
        }
        return new ImmutablePair<>(rs1, rs2);
    }

    public List<Pair<String, Integer>> getAllOccupationsShareByStars(int stars) {
        Query nativeQuery = entityManager.createNativeQuery(ALL_OCCUPATIONS_SHARE_BY_STARS);
        nativeQuery.setParameter(1, stars);
      //  List<Pair<String, Integer>> occupationsByStars = new ArrayList<>();
        //noinspection unchecked
        List<Object[]> resultList = nativeQuery.getResultList();
        return (resultList.stream().map(row ->
                new ImmutablePair<>(
                        (String) row[0],
                        ((BigInteger) row[1]).intValue())).collect(Collectors.toList()));
    }

    public List<Pair<String, Integer>> getAllAgesShareByStars(int stars) {
        Query nativeQuery = entityManager.createNativeQuery(ALL_AGES_SHARE_BY_STARS);
        nativeQuery.setParameter(1, stars);
        //noinspection unchecked
        List<Object[]> resultList = nativeQuery.getResultList();
        return (resultList.stream().map(row ->
                new ImmutablePair<>(
                        (String) row[0],
                        ((BigInteger) row[1]).intValue())).collect(Collectors.toList()));
    }

    public List<Pair<String, Integer>> getAllGendersShareByStars(int stars) {
        Query nativeQuery = entityManager.createNativeQuery(ALL_GENDERS_SHARE_BY_STARS);
        nativeQuery.setParameter(1, stars);

        List<Object[]> resultList = nativeQuery.getResultList();
        return (resultList.stream().map(row ->
        new ImmutablePair<>(
                (Boolean)row[0]?"Male":"Female",
                ((BigInteger) row[1]).intValue())).collect(Collectors.toList())
        );
    }

    public int getCommonRatedMoviesCount(User user1, User user2) {
        Query nativeQuery = entityManager.createNativeQuery(COMMON_MOVIES_COUNT);
        nativeQuery.setParameter(1, user1.getId());
        nativeQuery.setParameter(2, user2.getId());

        return ((BigInteger) nativeQuery.getSingleResult()).intValue();
    }


    public Double getAverageRating(User user) {
        try {
            return entityManager.createNamedQuery(Rating.AVERAGE_RATED_FOR_USER, Double.class)
                    .setParameter("user", user)
                    .getSingleResult();
        } catch (PersistenceException ex) {
            return null;
        }
    }


    public Long getUserRatedCount(User user) {
        try {
            return entityManager.createNamedQuery(Rating.RATED_COUNT_FOR_USER, Long.class)
                    .setParameter("user", user)
                    .getSingleResult();
        } catch (PersistenceException ex) {
            return null;
        }
    }

    public Rating getRating(User user, Movie movie) {
        try {
            return entityManager.createNamedQuery(Rating.GET_RATING_USER_MOVIE, Rating.class)
                    .setParameter("user", user)
                    .setParameter("movie", movie)
                    .getSingleResult();
        } catch (PersistenceException ex) {
            return null;
        }
    }

    @Transactional
    public Long getCount() {
        try {
            return (Long) entityManager.createNamedQuery(Rating.GET_COUNT)
                    .getSingleResult();
        } catch (PersistenceException ex) {
            return null;
        }
    }

    @Transactional
    public Rating add(Rating rating) {
        try {
            entityManager.persist(rating);
            return rating;
        } catch (EntityExistsException e) {
            throw new IllegalArgumentException("The entity is already in DB, use update() instead", e);
        }
    }

    public Rating update(Rating rating) {
        return entityManager.merge(rating);
    }


}
