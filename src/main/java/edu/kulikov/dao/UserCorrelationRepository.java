package edu.kulikov.dao;

import edu.kulikov.models.User;
import edu.kulikov.models.UserCorrelation;
import edu.kulikov.services.MovieService;
import edu.kulikov.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityExistsException;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceException;
import java.util.List;

@Repository
public class UserCorrelationRepository {

    @PersistenceContext
    private EntityManager entityManager;

    @Autowired
    private UserService userService;
    @Autowired
    private MovieService movieService;


    public UserCorrelation getByTwoUsers(User user1, User user2) {
        try {
            return entityManager.createNamedQuery(UserCorrelation.FIND_BY_TWO_USERS, UserCorrelation.class)
                    .setParameter("user1", user1)
                    .setParameter("user2", user2)
                    .getSingleResult();
        } catch (PersistenceException e) {
            return null;
        }
    }

    public List<UserCorrelation> getAllByUser(User user) {
        try {
            return entityManager.createNamedQuery(UserCorrelation.FIND_ALL_BY_USER, UserCorrelation.class)
                    .setParameter("user", user)
                    .getResultList();
        } catch (PersistenceException e) {
            e.printStackTrace();
            return null;
        }
    }

    public List<UserCorrelation> getTopNByUser(User user, int n) {
        try {
            return entityManager.createNamedQuery(UserCorrelation.GET_TOP_N_BY_USER, UserCorrelation.class)
                    .setParameter("user", user)
                    .setMaxResults(n)
                    .getResultList();
        } catch (PersistenceException e) {
            e.printStackTrace();
            return null;
        }
    }


    @Transactional
    public void add(UserCorrelation userCorrelation) {
        userCorrelation.orderUserIDs();
        userCorrelation.setUpdated(true);
        try {
            entityManager.persist(userCorrelation);
        } catch (EntityExistsException ex) {
            throw new IllegalArgumentException("The entity is already in DB, use update() instead", ex);
        }
    }

    @Transactional
    public void update(UserCorrelation userCorrelation) {
        userCorrelation.orderUserIDs();
        userCorrelation.setUpdated(true);
        entityManager.merge(userCorrelation);
    }


    @Transactional
    public void setCorrelationsOutdated(User user) {
        try {
            entityManager.createNamedQuery(UserCorrelation.SET_OUTDATED_FOR_USER)
                    .setParameter("user", user)
                    .executeUpdate();
        } catch (PersistenceException e) {
            e.printStackTrace();
        }
    }


    @Transactional
    public List<UserCorrelation> getOutdatedCorrelationsByUser(User user) {
        try {
            return entityManager
                    .createNamedQuery(UserCorrelation.GET_OUTDATED_FOR_USER, UserCorrelation.class)
                    .setParameter("user", user)
                    .getResultList();
        } catch (PersistenceException ex) {
            ex.printStackTrace();
            return null;
        }
    }
}
