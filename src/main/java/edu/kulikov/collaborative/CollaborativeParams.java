package edu.kulikov.collaborative;


public class CollaborativeParams {
    private int neighbours;
    private int minOverlappingItems;
    private int harmonicThreshold;
    private int selfWeightingThreshold;
    private int maxSelfWeighting;

    public CollaborativeParams(int neighbours, int minOverlappingItems, int harmonicThreshold, int selfWeightingThreshold, int maxSelfWeighting) {
        this.neighbours = neighbours;
        this.minOverlappingItems = minOverlappingItems;
        this.harmonicThreshold = harmonicThreshold;
        this.selfWeightingThreshold = selfWeightingThreshold;
        this.maxSelfWeighting = maxSelfWeighting;
    }

    public int getNeighbours() {
        return neighbours;
    }

    public int getMinOverlappingItems() {
        return minOverlappingItems;
    }


    public int getHarmonicThreshold() {
        return harmonicThreshold;
    }

    public int getSelfWeightingThreshold() {
        return selfWeightingThreshold;
    }

    public int getMaxSelfWeighting() {
        return maxSelfWeighting;
    }
}
