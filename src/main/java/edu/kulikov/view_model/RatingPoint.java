package edu.kulikov.view_model;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class RatingPoint {
    public String x;
    public int y;

    public RatingPoint(String x, int y) {
        this.x = x;
        this.y = y;
    }

    public String getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public static List<RatingPoint> createFromMapCount(Map<Integer, Long> ratings) {
        List<RatingPoint> result = ratings.entrySet().stream()
                .filter(entry -> entry.getKey() != null)
                .map(entry -> new RatingPoint(entry.getKey().toString(), entry.getValue().intValue()))
                .collect(Collectors.toList());
        return result;
    }
}