package edu.kulikov.view_model;

import org.hibernate.validator.constraints.*;

import edu.kulikov.models.Account;

public class SignupForm {

	private static final String NOT_BLANK_MESSAGE = "{notBlank.message}";

    @NotBlank(message = SignupForm.NOT_BLANK_MESSAGE)
	private String login;

    @NotBlank(message = SignupForm.NOT_BLANK_MESSAGE)
	private String password;

	private int age;
	@NotBlank(message = SignupForm.NOT_BLANK_MESSAGE)
	private String occupation;
	private boolean male;

    public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public Account createAccount() {
        return new Account(getLogin(), getPassword(), "ROLE_USER");
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	public String getOccupation() {
		return occupation;
	}

	public void setOccupation(String occupation) {
		this.occupation = occupation;
	}

	public boolean isMale() {
		return male;
	}

	public void setMale(boolean isMale) {
		this.male = isMale;
	}
}
