package edu.kulikov.view_model;

/**
 * Created by Andrey on 02.03.2015.
 */
public class Page {
    private String text;
    private String number;
    private boolean active;
    private boolean enabled;

    public Page(String text, String number, boolean active, boolean enabled) {
        this.text = text;
        this.number = number;
        this.active = active;
        this.enabled = enabled;
    }

    public String getText() {
        return text;
    }

    public String getNumber() {
        return number;
    }

    public boolean isActive() {
        return active;
    }

    public boolean isEnabled() {
        return enabled;
    }
}
