package edu.kulikov.view_model;

import edu.kulikov.models.User;

public class UserWithRatings {
    private User user;
    private Double avgRating;
    private Integer ratingsCount;

    public UserWithRatings(User user, Double avgRating, Integer ratingsCount) {
        this.user = user;
        this.avgRating = avgRating;
        this.ratingsCount = ratingsCount;
    }

    public User getUser() {
        return user;
    }

    public Double getAvgRating() {
        return avgRating;
    }

    public Integer getRatingsCount() {
        return ratingsCount;
    }
}
