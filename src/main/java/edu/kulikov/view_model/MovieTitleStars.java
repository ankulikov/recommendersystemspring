package edu.kulikov.view_model;

public class MovieTitleStars {
    public int movieId;
    public String movieTitle;

    public int stars;

    public MovieTitleStars(int movieId, String movieTitle, int stars) {
        this.movieTitle = movieTitle;
        this.stars = stars;
        this.movieId = movieId;
    }
}
