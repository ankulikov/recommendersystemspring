package edu.kulikov.view_model;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

public class MetricForm {
    @NotNull(message = "Значение не может быть пустым!")
    @Min(value = 1,message = "Значение не может быть меньше 1")
    private Integer userId = 1;
    @NotNull(message = "Значение не может быть пустым!")
    @Min(value = 1, message = "Значение не может быть меньше 1")
    @Max(value = 99, message = "Значение не может быть больше 99")
    private Integer splitter = 80;
    @NotNull(message = "Значение не может быть пустым!")
    private Double start = 2.1;
    @NotNull(message = "Значение не может быть пустым!")
    private Double end = 4.2;
    @NotNull(message = "Значение не может быть пустым!")
    @Min(value = 1,message = "Значение не может быть меньше 1")
    private Integer count = 15;

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public Integer getSplitter() {
        return splitter;
    }

    public void setSplitter(Integer splitter) {
        this.splitter = splitter;
    }

    public Double getStart() {
        return start;
    }

    public void setStart(Double start) {
        this.start = start;
    }

    public Double getEnd() {
        return end;
    }

    public void setEnd(Double end) {
        this.end = end;
    }

    public Integer getCount() {
        return count;
    }

    public void setCount(Integer count) {
        this.count = count;
    }
}
