package edu.kulikov.services;

import com.google.common.util.concurrent.AtomicDouble;
import edu.kulikov.models.Movie;
import edu.kulikov.models.Prediction;
import edu.kulikov.models.User;
import org.apache.commons.collections4.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import weka.core.Instances;

import java.util.HashMap;
import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;

@Service
public class ContentPredictionServiceExecutor {
    @Autowired
    private PredictionService predictionService;
    @Autowired
    private MovieService movieService;
    @Autowired
    private UserService userService;
    @Autowired
    private RatingService ratingService;
    @Autowired
    private BayesClassifierService bayesClassifierService;

    private final AtomicDouble progress = new AtomicDouble();
    private final AtomicInteger currentMovie = new AtomicInteger();
    private final AtomicInteger currentUser = new AtomicInteger();
    private final AtomicBoolean inProgress = new AtomicBoolean(false);
    private final AtomicBoolean interrupted = new AtomicBoolean(false);

    @Async
    public void doContentPrediction() throws Exception {
        if (inProgress.get()) {
            System.out.println("already in progress");
            return;
        }
        clearState();
        inProgress.set(true);
        Long usersCount = userService.getCount();
        Long movieCount = movieService.getCount();
        double total = usersCount * movieCount;
        List<User> outdatedUsers = userService.getAllWithOutdatedContentPredictions();
        //count share of updated predictions
        progress.addAndGet((usersCount - outdatedUsers.size()) * movieCount / total);
        if (checkInterruption()) return;

        for (User user : outdatedUsers) {
            //iterate over unrated movies without ones that have updated prediction for user
            List<Movie> movies = getMoviesForPredictions(user.getId());
            progress.addAndGet((movieCount - movies.size()) / total);
            currentUser.set(user.getId());
            if (checkInterruption()) return;
            if (movies.isEmpty()) { //impossible situation, only for debugging
                System.out.println("Inconsistency in DB!");
                progress.addAndGet(movieCount / total);
                if (checkInterruption()) return;
            }
            Instances trainingSet = bayesClassifierService.loadTrainingSet(user.getId(), false);
            bayesClassifierService.trainClassifier(trainingSet);

            for (Movie movie : movies) {
                System.out.println("User: " + user.getId() + " Movie:" + movie.getId());
                if (checkInterruption()) return;

                Instances testSet = bayesClassifierService.loadTestSet(movie.getId());
                HashMap<String, Double> distribution = bayesClassifierService.getClassesPrediction(testSet);
                Prediction prediction = predictionService.getPredictionByDistribution(user.getId(), movie.getId(), distribution);
                prediction.setContentUpdated(true);
                predictionService.save(prediction);

                progress.addAndGet(1.0 / total);
                currentMovie.set(movie.getId());
            }
            user.setContentPredictionUpdated(true);
            userService.save(user);
        }
        progress.set(1);
        inProgress.set(false);


    }

    private List<Movie> getMoviesForPredictions(Integer userId) {
        List<Movie> movies = CollectionUtils.subtract(
                movieService.getUnratedMovies(userId),
                predictionService
                        .getUpdatedPredictionsForUser(userId)
                        .stream()
                        .map(Prediction::getMovie)
                        .collect(Collectors.toList())).stream().collect(Collectors.toList());
        return movies;
    }

    private boolean checkInterruption() {
        if (interrupted.get()) {
            System.out.println("is interrupted");
            clearState();
            return true;
        }
        return false;
    }

    private void clearState() {
        inProgress.set(false);
        interrupted.set(false);
        progress.set(0);
    }

    public void stop() {
        interrupted.set(true);
    }

    public Double getProgress() {
        if (progress == null)
            return 0d;
        return progress.get();
    }

    public Integer getCurrentMovieId() {
        return currentMovie.get();
    }

    public Integer getCurrentUserId() {
        return currentUser.get();
    }


}
