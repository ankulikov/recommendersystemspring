package edu.kulikov.services;

import edu.kulikov.dao.MovieRepository;
import edu.kulikov.dao.UserRepository;
import edu.kulikov.models.Movie;
import edu.kulikov.models.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.EnumSet;
import java.util.List;


@Service
public class MovieService {
    @Autowired
    private MovieRepository movieRepository;
    @Autowired
    private UserService userService;

    public Movie getById(int id, EnumSet<Movie.LAZY> fields) {
        return movieRepository.getById(id, fields);
    }

    public List<Movie> getAll(int count, int startIndex, EnumSet<Movie.LAZY> fields) {
        return movieRepository.getAll(count, startIndex, fields);
    }

    public List<Movie> getAll(EnumSet<Movie.LAZY> fields) {
        return movieRepository.getAll(fields);
    }

    @Transactional
    public List<Movie> getUnratedMovies(int userID) {
        User user = userService.getById(userID, EnumSet.noneOf(User.LAZY.class));
        List<Movie> unratedMovies = movieRepository.getUnratedMovies(user);
        return unratedMovies;

    }

    public Long getCount() {
        return movieRepository.getCount();
    }

    public void initialize(Movie movie, EnumSet<Movie.LAZY> fields) {
        movieRepository.initialize(movie, fields);
    }

}
