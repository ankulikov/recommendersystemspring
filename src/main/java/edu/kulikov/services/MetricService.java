package edu.kulikov.services;

import edu.kulikov.dao.MetricRepository;
import edu.kulikov.models.Metric;
import edu.kulikov.models.User;
import org.apache.commons.lang3.tuple.Pair;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class MetricService {
    @Autowired
    private MetricRepository metricRepository;
    @Autowired
    private UserService userService;

    /**
     * Computes ROC-curve points (TPR; FPR) with different thresholds
     *
     * @param origUserId     user with full set of ratings
     * @param testUserId     user with partial set of ratings
     * @param startThreshold starting threshold value
     * @param endThreshold   ending threshold value
     * @param count          count of threshold values between starting and ending (including boundaries)
     * @return List of MetricPoint
     */
    public List<MetricPoint> getRocValues(Integer origUserId, Integer testUserId,
                                          Double startThreshold, Double endThreshold,
                                          Integer count) {
        List<Pair<Integer, Integer>> realPredictedResults
                = metricRepository.getRealAndPredicted(origUserId, testUserId);
        List<MetricPoint> result = new ArrayList<>();
        double delta = (endThreshold - startThreshold) / (count-1);
        double value = startThreshold;
        for (int i = 0; i < count; i++, value += delta) {
            result.add(getRocValue(realPredictedResults, value));
        }
        return result;
    }

    private MetricPoint getRocValue(List<Pair<Integer, Integer>> realPredictedResults,
                                    Double threshold) {
        long tp = realPredictedResults.stream()
                .filter(p -> p.getLeft() >= threshold && p.getRight() >= threshold).count();
        long fp = realPredictedResults.stream()
                .filter(p -> p.getLeft() < threshold && p.getRight() >= threshold).count();

        long positive = realPredictedResults.stream().filter(p -> p.getLeft() >= threshold).count();
        long negative = realPredictedResults.stream().filter(p -> p.getLeft() < threshold).count();
        return new MetricPoint((double) tp / positive, (double) fp / negative, threshold);
    }

    public Metric setMetricPoints(Metric metric, List<MetricPoint> points) {
        List<Double> tprs = new ArrayList<>();
        List<Double> fprs = new ArrayList<>();
        List<Double> thresholds = new ArrayList<>();
        points.forEach(p-> {
            tprs.add(p.getTpr());
            fprs.add(p.getFpr());
            thresholds.add(p.getThreshold());
        });
        metric.setTpr(tprs);
        metric.setFpr(fprs);
        metric.setThreshold(thresholds);
        return metric;
    }

    public Metric getById(Integer id) {
        return metricRepository.getById(id);
    }

    public List<Metric> getAll() {
        return metricRepository.getAll();
    }

    public Metric save(Metric metric) {
        return metricRepository.save(metric);
    }

    public static class MetricPoint {
        private Double tpr;
        private Double fpr;
        private Double threshold;

        public MetricPoint(Double tpr, Double fpr, Double threshold) {
            this.tpr = tpr;
            this.fpr = fpr;
            this.threshold = threshold;
        }

        public Double getTpr() {
            return tpr;
        }

        public void setTpr(Double tpr) {
            this.tpr = tpr;
        }

        public Double getFpr() {
            return fpr;
        }

        public void setFpr(Double fpr) {
            this.fpr = fpr;
        }

        public Double getThreshold() {
            return threshold;
        }

        public void setThreshold(Double threshold) {
            this.threshold = threshold;
        }
    }
}
