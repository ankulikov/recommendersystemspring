package edu.kulikov.services;

import com.google.common.util.concurrent.AtomicDouble;
import edu.kulikov.models.User;
import edu.kulikov.models.UserCorrelation;
import org.apache.commons.lang3.tuple.Pair;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicBoolean;

@Service
public class UserCorrelationServiceExecutor {

    @Autowired
    private UserCorrelationService correlationService;
    @Autowired
    private RatingService ratingService;
    @Autowired
    private UserService userService;
    @Autowired
    private PredictionService predictionService;

    private final ConcurrentHashMap<Integer, ExecutorItem> tasks;

    public UserCorrelationServiceExecutor() {
        tasks = new ConcurrentHashMap<>();
    }


    @Async
    public void doUserCorrelation(int userID) {
        if (tasks.get(userID) != null && tasks.get(userID).inProgress.get())
            return;
        tasks.put(userID, new ExecutorItem());

        double[] stars1 = transformMovieStarList(predictionService.getAllPseudoStars(userID));

        List<User> users = userService.getAll(null);
        for (User user : users) {
            if (checkInterruption(userID)) {
                clearState(userID);
                return;
            }
            if (user.getId() != userID) {
                UserCorrelation correlation = correlationService.getByTwoUsers(userID, user.getId());
                if (correlation == null || !correlation.isUpdated()) {

                    double[] stars2 = transformMovieStarList(predictionService.getAllPseudoStars(user.getId()));
                    UserCorrelation computedCorr = correlationService.computeUserCorrelation(userID, user.getId(), stars1, stars2);
                    correlationService.save(computedCorr);
                }
            }
            tasks.get(userID).progress.addAndGet(1 / (double) users.size());
        }
        tasks.get(userID).progress.set(1);
        clearState(userID);
    }


    public Double getProgress(int userID) {
        ExecutorItem item = tasks.get(userID);
        if (item != null && item.progress != null) {
            return item.progress.get();
        }
        return 0d;
    }

    public void stop(int userId) {
        ExecutorItem item = tasks.get(userId);
        if (item != null) {
            item.isInterrupted.set(true);
        }
    }

    private boolean checkInterruption(int userId) {
        ExecutorItem item = tasks.get(userId);
        return item != null && item.isInterrupted.get();
    }

    private void clearState(int userId) {
        ExecutorItem item = tasks.get(userId);
        if (item != null) {
            item.inProgress.set(false);
            item.isInterrupted.set(false);
        }
    }

    private double[] transformMovieStarList(List<Pair<Integer, Byte>> movieStarsList) {
        return movieStarsList.stream().sorted((a, b) -> Integer.compare(a.getLeft(), b.getLeft()))
                .mapToDouble(Pair::getRight).toArray();
    }

    private class ExecutorItem {
        public AtomicBoolean inProgress = new AtomicBoolean(false);
        public AtomicBoolean isInterrupted = new AtomicBoolean(false);
        public AtomicDouble progress = new AtomicDouble(0);
    }
}
