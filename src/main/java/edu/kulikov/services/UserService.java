package edu.kulikov.services;


import edu.kulikov.dao.UserRepository;
import edu.kulikov.models.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.EnumSet;
import java.util.List;

/**
 * Service for users loaded from data (not accounts)
 */
@Service
public class UserService {
    @Autowired
    UserRepository userRepository;

    public User getById(int id, EnumSet<User.LAZY> fields) {
        return userRepository.getById(id, fields);
    }

    public User getByLogin(String login, EnumSet<User.LAZY> fields) {
        return userRepository.getByLogin(login, fields);
    }

    public List<User> getAll(int count, int startIndex, EnumSet<User.LAZY> fields) {
        return userRepository.getAll(count, startIndex, fields);
    }

    public List<User> getAll(EnumSet<User.LAZY> fields) {
        return userRepository.getAll(fields);
    }

    public List<User> getAllWithOutdatedContentPredictions() {
        return userRepository.getAllWithOutdatedContentPredictions();
    }

    public Long getCount() {
        return userRepository.getCount();
    }

    public User save(User user) {
        return userRepository.save(user);
    }

    @Transactional
    public void delete(Integer id) {
        User user = getById(id, null);
        if (user != null)
            userRepository.delete(user);
    }

}
