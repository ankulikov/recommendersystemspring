package edu.kulikov.services;

import edu.kulikov.bayes.BayesDataCreator;
import edu.kulikov.models.Movie;
import edu.kulikov.models.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import weka.classifiers.Classifier;
import weka.classifiers.bayes.NaiveBayesMultinomialText;
import weka.core.Attribute;
import weka.core.Instances;
import weka.core.converters.ConverterUtils;
import weka.core.tokenizers.WordTokenizer;

import java.io.File;
import java.io.IOException;
import java.util.EnumSet;
import java.util.HashMap;

@Service
public class BayesClassifierService {
    @Autowired
    private MovieService movieService;
    @Autowired
    private UserService userService;
    @SuppressWarnings("SpringJavaAutowiringInspection")
    @Autowired
    private BayesDataCreator bayesDataCreator;
    private NaiveBayesMultinomialText bayesClassifier;

    {
        setUpBayesClassifier();
        System.out.println("Classifier created!");
    }


    public Instances loadTrainingSet(int userId,
                                     boolean setFromCache) throws Exception {
        File rated = bayesDataCreator.exportRatedMovies(userId, setFromCache);
        ConverterUtils.DataSource source = new ConverterUtils.DataSource(rated.getAbsolutePath());
        Instances trainingSet = source.getDataSet();
        trainingSet.setClassIndex(trainingSet.numAttributes() - 1);
        return trainingSet;
    }

    public Instances loadTestSet(int movieId) throws Exception {
        File unrated = bayesDataCreator.exportUnratedMovie(movieId);
        ConverterUtils.DataSource source = new ConverterUtils.DataSource(unrated.getAbsolutePath());
        Instances testSet = source.getDataSet();
        return testSet;
    }

    public void trainClassifier(Instances trainSet) throws Exception {
        bayesClassifier.buildClassifier(trainSet);
    }

    public HashMap<String, Double> getClassesPrediction(Instances testSet) throws Exception {
        double[] distributionForInstance =
                bayesClassifier.distributionForInstance(testSet.instance(0));
        return getClassDistribution(testSet.classAttribute(), distributionForInstance);

    }

    private HashMap<String, Double> getClassDistribution(Attribute classAttribute, double[] distribution) {
        HashMap<String, Double> classDistribution = new HashMap<>();
        for (int i = 0; i < distribution.length; i++) {
            classDistribution.put(classAttribute.value(i), distribution[i]);
        }
        return classDistribution;
    }

    private void setUpBayesClassifier() {
        if (bayesClassifier == null) {
            bayesClassifier = new NaiveBayesMultinomialText();
        }
        bayesClassifier.setUseWordFrequencies(true);
        bayesClassifier.setMinWordFrequency(1.0);
        bayesClassifier.setLNorm(2.0);
        bayesClassifier.setNorm(1.0);
        WordTokenizer wordTokenizer = new WordTokenizer();
        wordTokenizer.setDelimiters(";");
        bayesClassifier.setTokenizer(wordTokenizer);
    }

}
