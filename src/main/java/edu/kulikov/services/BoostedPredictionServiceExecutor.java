package edu.kulikov.services;

import com.google.common.util.concurrent.AtomicDouble;
import edu.kulikov.models.Movie;
import edu.kulikov.models.Prediction;
import edu.kulikov.models.User;
import edu.kulikov.models.UserCorrelation;
import edu.kulikov.view_model.BoostedPredictionProgress;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;

@Service
public class BoostedPredictionServiceExecutor {
    @Autowired
    private PredictionService predictionService;
    @Autowired
    private MovieService movieService;
    @Autowired
    private UserCorrelationService correlationService;
    @Autowired
    private UserService userService;

    private final ConcurrentHashMap<Integer, ExecutorItem> tasks;

    public BoostedPredictionServiceExecutor() {
        tasks = new ConcurrentHashMap<>();
    }

    @Async
    public void doBoostedPrediction(int userID) {
        if (tasks.get(userID) != null && tasks.get(userID).inProgress.get()) {
            return;
        }
        User user = userService.getById(userID, null);
        tasks.put(userID, new ExecutorItem());
        tasks.get(userID).inProgress.set(true);
        List<Movie> unratedMovies = movieService.getUnratedMovies(userID);
        double delta = 1.0 / unratedMovies.size();
        List<UserCorrelation> neighbors = correlationService.getTopNbyUser(userID, true);
        for (Movie movie : unratedMovies) {
            if (checkInterruption(userID)) {
                clearState(userID);
                return;
            }
            tasks.get(userID).movie = movie.getId();
            tasks.get(userID).progress.addAndGet(delta);
            double boostedPrediction = predictionService.computeBoostedPrediction(userID, movie.getId(), neighbors);
            Prediction prediction = predictionService.getPredictionUserMovie(userID, movie.getId());
            prediction.setBoostedPrediction(boostedPrediction);
            predictionService.save(prediction);
        }
        clearState(userID);
        tasks.get(userID).progress.set(1);
        user.setRecommended(true);
        userService.save(user);
    }

    public Double getProgress(int userID) {
        ExecutorItem item = tasks.get(userID);
        if (item != null && item.progress != null) {
            return item.progress.get();
        }
        return 0d;
    }

    public Integer getMovie(int userID) {
        ExecutorItem item = tasks.get(userID);
        if (item != null) {
            Integer movie = item.movie;
            return movie;
        }
        return null;
    }

    private boolean checkInterruption(int userID) {
        ExecutorItem item = tasks.get(userID);
        return item != null && item.isInterrupted.get();
    }

    private void clearState(int userID) {
        ExecutorItem item = tasks.get(userID);
        if (item != null) {
            item.inProgress.set(false);
            item.isInterrupted.set(false);
        }
    }

    public void stop(int userID) {
        ExecutorItem item = tasks.get(userID);
        if (item != null) {
            item.isInterrupted.set(true);
        }
    }

    private class ExecutorItem {
        public int movie;
        public AtomicBoolean inProgress = new AtomicBoolean(false);
        public AtomicBoolean isInterrupted = new AtomicBoolean(false);
        public AtomicDouble progress = new AtomicDouble();
    }

}
