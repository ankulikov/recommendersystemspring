package edu.kulikov.services;

import edu.kulikov.dao.PredictionRepository;
import edu.kulikov.models.Movie;
import edu.kulikov.models.Prediction;
import edu.kulikov.models.User;
import edu.kulikov.models.UserCorrelation;
import org.apache.commons.lang3.tuple.Pair;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class PredictionService {
    @Autowired
    private PredictionRepository predictionRepo;
    @Autowired
    private UserService userService;
    @Autowired
    private MovieService movieService;
    @Autowired
    private UserCorrelationService usrCorrService;
    @Autowired
    private RatingService ratingService;

    @Transactional
    public Integer getPseudoStars(int userId, int movieId) {
        User user = userService.getById(userId, EnumSet.noneOf(User.LAZY.class));
        Movie movie = movieService.getById(movieId, EnumSet.noneOf(Movie.LAZY.class));
        return predictionRepo.getPseudoStars(user, movie);
    }

    public List<Pair<Integer, Byte>> getAllPseudoStars(int userId) {
        return predictionRepo.getAllPseudoStars(userId);
    }

    @Transactional
    public Double getAveragePseudoStarsForUser(int userId) {
        User user = userService.getById(userId, EnumSet.noneOf(User.LAZY.class));
        return predictionRepo.getAveragePseudoStarsForUser(user);
    }

    @Transactional
    public Prediction getPredictionUserMovie(int userId, int movieId) {
        User user = userService.getById(userId, EnumSet.noneOf(User.LAZY.class));
        Movie movie = movieService.getById(movieId, EnumSet.noneOf(Movie.LAZY.class));
        return predictionRepo.getPredictionUserMovie(user, movie);
    }

    public List<Prediction> getTopNPredictionsForUser(int userId, int n) {
        User user = userService.getById(userId, EnumSet.noneOf(User.LAZY.class));
        return predictionRepo.getTopNPredictionsForUser(user, n);
    }


    public List<Prediction> getUpdatedPredictionsForUser(int userId) {
        User user = userService.getById(userId, EnumSet.noneOf(User.LAZY.class));
        return predictionRepo.getUpdatedPredictionsForUser(user);
    }

    public Prediction getPredictionByDistribution(int userId, int movieId,
                                                  HashMap<String, Double> distribution) {
        int stars;
        Map.Entry<String, Double> max =
                distribution
                        .entrySet()
                        .stream()
                        .max((e1, e2) -> Double.compare(e1.getValue(), e2.getValue()))
                        .get();
        try {
            stars = Integer.parseInt(max.getKey());
        } catch (NumberFormatException e) {
            throw new IllegalArgumentException("Unknown prediction class");
        }
        User user = userService.getById(userId, null);
        Movie movie = movieService.getById(movieId, null);

        Prediction prediction = new Prediction(user, movie);
        prediction.setContentPrediction(stars);
        prediction.setContentUpdated(true);
        return prediction;
    }

    public void setContentPredictionOutdatedForUser(Integer userID) {
        User user = userService.getById(userID, EnumSet.noneOf(User.LAZY.class));
        predictionRepo.setContentPredictionOutdatedForUser(user);
    }

    public Prediction save(Prediction prediction) {
        return predictionRepo.save(prediction);
    }

    public void delete(Prediction prediction) {
        predictionRepo.delete(prediction);
    }

    public double computeBoostedPrediction(Integer userId, Integer movieId,
                                           List<UserCorrelation> neighbors) {
        Double currAvgRating = getAveragePseudoStarsForUser(userId);
        double numerator, denumerator;
        double selfWeight = ratingService.computeSelfWeighting(userId);
        //content predictions should be computed before
        numerator = selfWeight * (getPredictionUserMovie(userId, movieId)
                .getContentPrediction() - currAvgRating);
        denumerator = selfWeight;


        for (UserCorrelation neighbor : neighbors) {
            double weightedCorr = neighbor.getHybridWeight() * neighbor.getCorrelation();
            numerator += weightedCorr *
                    (getPseudoStars(neighbor.getUser2().getId(), movieId) -
                            getAveragePseudoStarsForUser(neighbor.getUser2().getId()));
            denumerator += weightedCorr;
        }

        return currAvgRating + numerator / denumerator;
    }


    @Transactional
    @Deprecated
    public double computeBoostedPrediction(Integer userID, Integer movieID, boolean fromCache) {
        Double currAvgRating = getAveragePseudoStarsForUser(userID);
        double numerator, denumerator;
        double selfWeight = ratingService.computeSelfWeighting(userID);
        //content predictions should be computed before
        numerator = selfWeight * (getPredictionUserMovie(userID, movieID)
                .getContentPrediction() - currAvgRating);
        denumerator = selfWeight;

        //correlations should be computed before
        List<UserCorrelation> nears = usrCorrService.getTopNbyUser(userID, fromCache);
        for (UserCorrelation near : nears) {
            if (near.getUser1().getId() != userID)
                throw new IllegalArgumentException("User 1 != userid");
            double coef = usrCorrService.getHybridCorrelationWeight(
                    near.getUser1().getId(),
                    near.getUser2().getId()) * near.getCorrelation();
            numerator += coef * (getPseudoStars(near.getUser2().getId(), movieID) -
                    getAveragePseudoStarsForUser(near.getUser2().getId()));
            denumerator += coef;
        }
        return currAvgRating + numerator / denumerator;
    }


}
