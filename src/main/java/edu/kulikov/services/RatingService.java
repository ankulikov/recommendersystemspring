package edu.kulikov.services;

import edu.kulikov.collaborative.CollaborativeParams;
import edu.kulikov.dao.RatingRepository;
import edu.kulikov.models.Movie;
import edu.kulikov.models.Prediction;
import edu.kulikov.models.Rating;
import edu.kulikov.models.User;
import org.apache.commons.lang3.tuple.Pair;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.EnumSet;
import java.util.List;

@Service
public class RatingService {
    @Autowired
    private RatingRepository ratingRepo;
    @Autowired
    private UserService userService;
    @Autowired
    private MovieService movieService;
    @Autowired
    private UserCorrelationService correlationService;
    @Autowired
    private PredictionService predictionService;
    @SuppressWarnings("SpringJavaAutowiringInspection")
    @Autowired
    private CollaborativeParams params;

    /**
     * Get pair of ratings for movies rated by two users (predicted ratings aren't included)
     *
     * @param userID1 first user ID
     * @param userID2 second user ID
     * @return pair of list of ratings for two users containing ratings for common movies
     */
    public Pair<List<Rating>, List<Rating>> getCommonRatedMoviesRatings(Integer userID1, Integer userID2) {
        User user1 = userService.getById(userID1, EnumSet.noneOf(User.LAZY.class));
        User user2 = userService.getById(userID2, EnumSet.noneOf(User.LAZY.class));
        return ratingRepo.getCommonMoviesRatings(user1, user2);
    }

    public int getCommonRatedMoviesCount(Integer userID1, Integer userID2) {
        User user1 = userService.getById(userID1, EnumSet.noneOf(User.LAZY.class));
        User user2 = userService.getById(userID2, EnumSet.noneOf(User.LAZY.class));
        return ratingRepo.getCommonRatedMoviesCount(user1, user2);
    }

    public List<Rating> getAllByUser(Integer userID) {
        User user = userService.getById(userID, EnumSet.of(User.LAZY.RATINGS));
        return user.getRatings();
    }

    public Double getAverageRating(Integer userID) {
        User user = userService.getById(userID, EnumSet.noneOf(User.LAZY.class));
        return ratingRepo.getAverageRating(user);
    }

    @Transactional
    public Rating getRating(Integer userID, Integer movieID) {
        Movie movie = movieService.getById(movieID, null);
        User user = userService.getById(userID, EnumSet.noneOf(User.LAZY.class));
        return ratingRepo.getRating(user, movie);
    }

    public List<Pair<String, Integer>> getAllOccupationsShareByStars(int stars) {
        return ratingRepo.getAllOccupationsShareByStars(stars);
    }

    public List<Pair<String, Integer>> getAllAgesShareByStars(int stars) {
        return ratingRepo.getAllAgesShareByStars(stars);
    }

    public List<Pair<String, Integer>> getAllGendersShareByStars(int stars) {
        return ratingRepo.getAllGendersShareByStars(stars);
    }

    public Long getUserRatedCount(Integer userID) {
        User user = userService.getById(userID, EnumSet.noneOf(User.LAZY.class));
        return ratingRepo.getUserRatedCount(user);
    }


    public Double computeSelfWeighting(Integer userID) {
        User user = userService.getById(userID, EnumSet.noneOf(User.LAZY.class));
        Long userRatedCount = ratingRepo.getUserRatedCount(user);
        if (userRatedCount < params.getSelfWeightingThreshold())
            return userRatedCount / (double) params.getSelfWeightingThreshold();
        return (double) params.getMaxSelfWeighting();
    }

    @Transactional
    public void save(Rating rating) {
        User user = rating.getUser();
        Rating currRating = getRating(user.getId(), rating.getMovie().getId());
        if (currRating == null) {
            ratingRepo.add(rating);
        } else {
            ratingRepo.update(rating);
        }
        //System.out.println(rating.getUser());
        predictionService.delete(new Prediction(user, rating.getMovie()));
        correlationService.setCorrelationsOutdated(user.getId());
        user.setContentPredictionUpdated(false);
        userService.save(user);
    }


    public Long getCount() {
        return ratingRepo.getCount();
    }
}
