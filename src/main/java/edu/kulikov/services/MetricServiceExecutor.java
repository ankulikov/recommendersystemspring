package edu.kulikov.services;

import edu.kulikov.models.Metric;
import edu.kulikov.models.Rating;
import edu.kulikov.models.User;
import edu.kulikov.models.UserCorrelation;
import edu.kulikov.view_model.MetricForm;
import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import java.util.Collections;
import java.util.List;
import java.util.Queue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.atomic.AtomicBoolean;

@Service
public class MetricServiceExecutor {
    @Autowired
    private UserService userService;
    @Autowired
    private RatingService ratingService;
    @Autowired
    private UserCorrelationService correlationService;
    @Autowired
    private MetricService metricService;
    @Autowired
    private ContentPredictionServiceExecutor contentExecutor;
    @Autowired
    private UserCorrelationServiceExecutor correlationExecutor;
    @Autowired
    private BoostedPredictionServiceExecutor boostedExecutor;

    private final Queue<MetricForm> tasks = new LinkedBlockingQueue<>();
    private final AtomicBoolean isInProgress = new AtomicBoolean(false);

    @Async
    public void addToQueue(MetricForm metricForm) throws Exception {
        tasks.add(metricForm);
        processNextItem();
    }

    private synchronized void processNextItem() {
        if (isInProgress.get())
            return;
        MetricForm item = tasks.poll();
        if (item != null) {
            doRocTest(item);
        }
    }

    private void doRocTest(MetricForm metricForm) {
        System.out.println("======BEGIN======");
        System.out.println("user:" + metricForm.getUserId());
        System.out.println("splitter:" + metricForm.getSplitter());
        isInProgress.set(true);
        User origUser = userService.getById(metricForm.getUserId(), null);
        User testUser = createTestUser();
        System.out.println("User created!");
        try {
            Pair<List<Rating>, List<Rating>> trainTestRatings =
                    splitTrainTest(metricForm.getUserId(), metricForm.getSplitter());
            System.out.println("Ratings splitted!");
            writeTrainRatings(testUser, trainTestRatings.getLeft());
            System.out.println("Ratings written!");
            computeContentPredictions();
            System.out.println("Content prediction finished!");
            computeUserCorrelations(testUser.getId());
            //"remove" original user from neighbors
            correlationService.save(new UserCorrelation(origUser, testUser, -1));
            correlationService.getTopNbyUser(testUser.getId(), false);   //update cache
            System.out.println("Correlations finished!");
            computeBoostedPredictions(testUser.getId());
            System.out.println("Boosted predictions finished!");
            computeAndSaveRoc(origUser, testUser,
                    trainTestRatings.getLeft().size(),
                    trainTestRatings.getRight().size(),
                    metricForm);
            System.out.println("ROC computed!");

        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            userService.delete(testUser.getId());
            System.out.println("User deleted!");
            System.out.println("======END======");
            isInProgress.set(false);
            processNextItem();
        }
    }

    private Pair<List<Rating>, List<Rating>> splitTrainTest(Integer userID, Integer splitter) {
        List<Rating> ratings = ratingService.getAllByUser(userID);
        Collections.shuffle(ratings);
        int middle = (int) (splitter / 100.0 * ratings.size());
        return new ImmutablePair<>(ratings.subList(0, middle), ratings.subList(middle, ratings.size()));
    }

    private User createTestUser() {
        User user = new User(100, true, "tester");
        return userService.save(user);

    }

    private void writeTrainRatings(User testUser, List<Rating> ratings) {
        ratings.stream().map(r -> new Rating(testUser, r.getMovie(), r.getStars()))
                .forEach(ratingService::save);
    }

    private void computeContentPredictions() throws Exception {
        contentExecutor.doContentPrediction();
        while (contentExecutor.getProgress() != 1) {
            Thread.sleep(1000);
            System.out.println(
                    String.format("Progress: %f, user: %d, movie: %d",
                            contentExecutor.getProgress(),
                            contentExecutor.getCurrentUserId(),
                            contentExecutor.getCurrentMovieId()));
        }
    }

    private void computeUserCorrelations(Integer testUserId) throws InterruptedException {
        correlationExecutor.doUserCorrelation(testUserId);
        while (correlationExecutor.getProgress(testUserId) != 1) {
            Thread.sleep(1000);
            System.out.println(
                    String.format("Progress: %f",
                            correlationExecutor.getProgress(testUserId)));
        }
    }

    private void computeBoostedPredictions(Integer testUserId) throws InterruptedException {
        boostedExecutor.doBoostedPrediction(testUserId);
        while (boostedExecutor.getProgress(testUserId) != 1) {
            Thread.sleep(1000);
            System.out.println(
                    String.format("Progress: %f, movie: %d",
                            boostedExecutor.getProgress(testUserId),
                            boostedExecutor.getMovie(testUserId)));
        }
    }

    private void computeAndSaveRoc(User origUser, User testUser,
                                   Integer trainSetSize, Integer testSetSize,
                                   MetricForm metricForm) {

        List<MetricService.MetricPoint> rocValues = metricService.getRocValues(origUser.getId(), testUser.getId(), metricForm.getStart(),
                metricForm.getEnd(), metricForm.getCount());
        Metric metric = new Metric();
        metric.setUser(origUser);
        metric.setTrainSetCount(trainSetSize);
        metric.setTestSetCount(testSetSize);
        metricService.setMetricPoints(metric, rocValues);
        metricService.save(metric);
    }


}
