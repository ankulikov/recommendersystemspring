package edu.kulikov.services;

import edu.kulikov.collaborative.CollaborativeParams;
import edu.kulikov.dao.UserCorrelationRepository;
import edu.kulikov.models.User;
import edu.kulikov.models.UserCorrelation;
import org.apache.commons.math3.exception.MathIllegalArgumentException;
import org.apache.commons.math3.stat.correlation.PearsonsCorrelation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Service
public class UserCorrelationService {

    @Autowired
    UserCorrelationRepository userCorrelationRepository;
    @Autowired
    UserService userService;
    @Autowired
    MovieService movieService;
    @Autowired
    RatingService ratingService;
    @Autowired
    PredictionService predictionService;
    @SuppressWarnings("SpringJavaAutowiringInspection")
    @Autowired
    CollaborativeParams params;

    private final Map<Integer, List<UserCorrelation>> neighbors = new HashMap<>();

    @Transactional
    public UserCorrelation getByTwoUsers(Integer userID1, Integer userID2) {
        User user1 = userService.getById(userID1, EnumSet.noneOf(User.LAZY.class));
        User user2 = userService.getById(userID2, EnumSet.noneOf(User.LAZY.class));
        return userCorrelationRepository.getByTwoUsers(user1, user2);
    }

    public UserCorrelation computeUserCorrelation(Integer userId1, Integer userId2,
                                                  double[] ratings1, double[] ratings2) {
        double correlation;
        if (ratings1.length != ratings2.length) {
            System.err.println("Lists of ratings must have the same size: u1:" + userId1 + " u2:" + userId2);
            correlation = 0;
        } else {
            try {
                PearsonsCorrelation pearsonsCorrelation = new PearsonsCorrelation();
                correlation = pearsonsCorrelation.correlation(ratings1, ratings2);
            } catch (MathIllegalArgumentException ex) {
                correlation = 0;
            }
            if (Double.isNaN(correlation))
                correlation = 0;
        }

        UserCorrelation userCorrelation = new UserCorrelation();
        userCorrelation.setUser1(userService.getById(userId1, null));
        userCorrelation.setUser2(userService.getById(userId2, null));
        userCorrelation.setCorrelation(correlation);
        userCorrelation.setUpdated(true);
        userCorrelation.setHybridWeight(getHybridCorrelationWeight(userId1, userId2));

        return userCorrelation;
    }


    public List<UserCorrelation> getTopNbyUser(Integer userID, boolean fromCache) {
        if (!fromCache || neighbors.get(userID) == null) {
            User user = userService.getById(userID, EnumSet.noneOf(User.LAZY.class));
            List<UserCorrelation> topNByUser = userCorrelationRepository
                    .getTopNByUser(user, params.getNeighbours())
                    .stream().map(c->c.putToFirst(userID)).collect(Collectors.toList());
            neighbors.put(userID, topNByUser);
        }
        return neighbors.get(userID);
    }

    public List<UserCorrelation> getOutdatedCorrelationsByUser(Integer userID) {
        User user = userService.getById(userID, EnumSet.noneOf(User.LAZY.class));
        return userCorrelationRepository.getOutdatedCorrelationsByUser(user);
    }


    @Transactional
    public void addOrUpdateAllInDb(List<UserCorrelation> userCorrelations) {
        for (UserCorrelation userCorrelation : userCorrelations) {
            save(userCorrelation);
        }
    }

    @Transactional
    public void save(UserCorrelation userCorrelation) {
        UserCorrelation existed = userCorrelationRepository.getByTwoUsers(userCorrelation.getUser1(), userCorrelation
                .getUser2());
        if (existed != null)
            userCorrelationRepository.update(userCorrelation);
        else
            userCorrelationRepository.add(userCorrelation);
    }


    private Double getHarmonicMean(Integer userID1, Integer userID2) {
        Long ratingCount1 = ratingService.getUserRatedCount(userID1);
        Long ratingCount2 = ratingService.getUserRatedCount(userID2);

        double rated1 = (ratingCount1 < params.getHarmonicThreshold()) ?
                ratingCount1 / (double) params.getHarmonicThreshold() : 1;
        double rated2 = (ratingCount2 < params.getHarmonicThreshold()) ?
                ratingCount2 / (double) params.getHarmonicThreshold() : 1;

        return 2 * rated1 * rated2 / (rated1 + rated2);
    }

    public Double getHybridCorrelationWeight(Integer userID1, Integer userID2) {
        int ratedMoviesCount = ratingService.getCommonRatedMoviesCount(userID1, userID2);
        double sg = ratedMoviesCount < params.getMinOverlappingItems() ?
                ratedMoviesCount / (double) params.getMinOverlappingItems() : 1;
        double hm = getHarmonicMean(userID1, userID2);
        return sg + hm;
    }

    public void setCorrelationsOutdated(Integer userID) {
        User user = userService.getById(userID, EnumSet.noneOf(User.LAZY.class));
        userCorrelationRepository.setCorrelationsOutdated(user);
    }


}
