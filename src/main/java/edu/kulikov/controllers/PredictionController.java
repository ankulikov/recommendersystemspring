package edu.kulikov.controllers;

import edu.kulikov.models.Prediction;
import edu.kulikov.services.*;
import edu.kulikov.support.web.ValidationHelper;
import edu.kulikov.view_model.BoostedPredictionProgress;
import edu.kulikov.view_model.ContentPredictionProgress;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletResponse;
import java.util.List;

@Controller
@RequestMapping("/predictions")
public class PredictionController {

    @Autowired
    private PredictionService predictionService;
    @Autowired
    private UserService userService;
    @Autowired
    private UserCorrelationServiceExecutor correlationServiceExecutor;
    @Autowired
    private ContentPredictionServiceExecutor contentPredictionServiceExecutor;
    @Autowired
    private BoostedPredictionServiceExecutor boostedPredictionServiceExecutor;

    private static final ContentPredictionProgress contentPredictionProgress =
            new ContentPredictionProgress();
    private static final BoostedPredictionProgress boostedPredictionProgress =
            new BoostedPredictionProgress();

    @RequestMapping(value = {"", "/"})
    public ModelAndView index(@RequestParam String userID,
                              @RequestParam(defaultValue = "1") String step,
                              HttpServletResponse response) {
        Integer user = ValidationHelper.checkId(userID);

        switch (step) {
            case "1":
                return firstStep(user);
            case "2":
                return secondStep(user);
            case "3":
                return thirdStep(user);
            default:
                return firstStep(user);
        }
    }

    public ModelAndView firstStep(Integer userID) {
        ModelAndView modelAndView = new ModelAndView("prediction/prediction_step1");
        modelAndView.addObject("userID", userID);
        return modelAndView;
    }

    public ModelAndView secondStep(Integer userID) {
        ModelAndView modelAndView = new ModelAndView("prediction/prediction_step2");
        modelAndView.addObject("userID", userID);
        return modelAndView;
    }

    public ModelAndView thirdStep(Integer userID) {
        ModelAndView modelAndView = new ModelAndView("prediction/prediction_step3");
        modelAndView.addObject("userID", userID);
        return modelAndView;
    }


    @RequestMapping(value = "/content_prediction/trigger")
    @ResponseStatus(value = HttpStatus.OK)
    public void contentPredictionTrigger() throws Exception {
        contentPredictionServiceExecutor.doContentPrediction();
    }


    @RequestMapping(value = "/content_prediction/stop")
    @ResponseStatus(value = HttpStatus.OK)
    public void stopContentPrediction() {
        contentPredictionServiceExecutor.stop();
    }

    @RequestMapping(value = "/content_prediction/progress")
    @ResponseBody
    public ContentPredictionProgress getContentPredictionProgress() {
        contentPredictionProgress.progress =
                (int) Math.floor(contentPredictionServiceExecutor.getProgress() * 100);
        contentPredictionProgress.user =
                contentPredictionServiceExecutor.getCurrentUserId();
        contentPredictionProgress.movie = contentPredictionServiceExecutor.getCurrentMovieId();
        return contentPredictionProgress;
    }

    @RequestMapping(value = "/correlation/trigger")
    @ResponseStatus(value = HttpStatus.OK)
    public void correlationTrigger(@RequestParam Integer userID) {
        correlationServiceExecutor.doUserCorrelation(userID);
    }

    @RequestMapping(value = "/correlation/progress")
    @ResponseBody
    public Integer getCorrelationProgress(@RequestParam Integer userID) {
        Double progress = correlationServiceExecutor.getProgress(userID);
        System.out.println("correlationProgress: " + progress + ", user: " + userID);
        if (progress == null) return null;
        return Math.min((int) Math.ceil(progress * 100), 100);
    }

    @RequestMapping(value = "/correlation/stop")
    @ResponseBody
    public void correlationStop(@RequestParam Integer userID) {
        correlationServiceExecutor.stop(userID);
    }

    @RequestMapping(value = "/boosted_prediction/trigger")
    @ResponseStatus(value = HttpStatus.OK)
    public void boostedPredictionTrigger(@RequestParam Integer userID) {
        boostedPredictionServiceExecutor.doBoostedPrediction(userID);
    }

    @RequestMapping(value = "/boosted_prediction/progress")
    @ResponseBody
    public BoostedPredictionProgress getBoostedPredictionProgress(@RequestParam Integer userID) {
        boostedPredictionProgress.progress =
                (int) (Math.floor(boostedPredictionServiceExecutor.getProgress(userID) * 100));
        boostedPredictionProgress.movie = boostedPredictionServiceExecutor.getMovie(userID);
        System.out.println("Boosted prediction progress: p:"
                + boostedPredictionProgress.progress + " m:" + boostedPredictionProgress.movie);
        return boostedPredictionProgress;
    }

    @RequestMapping(value = "/boosted_prediction/stop")
    @ResponseStatus(value = HttpStatus.OK)
    public void stopBoostedPrediction(@RequestParam Integer userID) {
        boostedPredictionServiceExecutor.stop(userID);
    }

    @RequestMapping(value = "/recommendations")
    @ResponseBody
    public List<Prediction> getTopNRecommendations(@RequestParam Integer userID,
                                                   @RequestParam Integer n) {
        return predictionService.getTopNPredictionsForUser(userID, n);
    }


}
