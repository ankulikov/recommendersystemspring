package edu.kulikov.controllers;

import edu.kulikov.models.Movie;
import edu.kulikov.models.Rating;
import edu.kulikov.models.User;
import edu.kulikov.services.MovieService;
import edu.kulikov.services.RatingService;
import edu.kulikov.services.UserService;
import edu.kulikov.view_model.DonutPoint;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import java.security.Principal;
import java.util.EnumSet;
import java.util.List;
import java.util.stream.Collectors;

@Controller
@RequestMapping("/ratings")
public class RatingController {
    @Autowired
    private RatingService ratingService;
    @Autowired
    private UserService userService;
    @Autowired
    private MovieService movieService;

    @RequestMapping(value = {"","/"})
    public ModelAndView index() {
        return getByStars(1);
    }

    @RequestMapping(value = "/{stars:[\\d]+}")
    public ModelAndView getByStars(@PathVariable Integer stars) {
        List<DonutPoint> occupations = ratingService.getAllOccupationsShareByStars(stars).stream()
                .map(m -> new DonutPoint(m.getLeft(), m.getRight()))
                .collect(Collectors.toList());
        List<DonutPoint> ages = ratingService.getAllAgesShareByStars(stars).stream()
                .map(m -> new DonutPoint(m.getLeft(), m.getRight()))
                .collect(Collectors.toList());
        List<DonutPoint> genders = ratingService.getAllGendersShareByStars(stars).stream()
                .map(m->new DonutPoint(m.getLeft(), m.getRight()))
                .collect(Collectors.toList());
        ModelAndView modelAndView =new ModelAndView("rating/rating");
        modelAndView.addObject("stars",stars);
        modelAndView.addObject("occupations", occupations);
        modelAndView.addObject("ages", ages);
        modelAndView.addObject("genders", genders);
        return modelAndView;
    }

    @RequestMapping(value = "save", method = RequestMethod.POST)
    @ResponseStatus(HttpStatus.OK)
    @Transactional
    public void saveRating(@RequestParam Integer movie,
                           @RequestParam Integer stars,
                           HttpServletRequest request,
                           Principal principal) {
        System.out.println(principal);
        if (stars > 5) stars = 5;
        if (stars < 1) stars = 1;
        if (!request.isUserInRole("ROLE_ANONYMOUS")) {
            User userById = userService.getByLogin(principal.getName(), null);
            Movie movieById = movieService.getById(movie, EnumSet.noneOf(Movie.LAZY.class));
            Rating rating = new Rating(userById, movieById, stars);
            ratingService.save(rating);
        }

    }


}
