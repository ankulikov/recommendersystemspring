package edu.kulikov.controllers;

import edu.kulikov.services.MovieService;
import edu.kulikov.services.RatingService;
import edu.kulikov.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import java.security.Principal;

@Controller
public class HomeController {
    @Autowired
    private UserService userService;
    @Autowired
    private MovieService movieService;
    @Autowired
    private RatingService ratingService;

    @RequestMapping(value = {"index", "/"}, method = RequestMethod.GET)
    public ModelAndView index() {
        ModelAndView modelAndView = new ModelAndView("home/home");
        modelAndView.addObject("users", userService.getCount());
        modelAndView.addObject("movies", movieService.getCount());
        modelAndView.addObject("ratings", ratingService.getCount());
        return modelAndView;
    }
}
