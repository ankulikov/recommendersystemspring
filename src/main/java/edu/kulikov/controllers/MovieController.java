package edu.kulikov.controllers;

import edu.kulikov.models.Movie;
import edu.kulikov.models.Rating;
import edu.kulikov.models.User;
import edu.kulikov.services.AccountService;
import edu.kulikov.services.MovieService;
import edu.kulikov.services.RatingService;
import edu.kulikov.services.UserService;
import edu.kulikov.support.web.PaginationHelper;
import edu.kulikov.support.web.ValidationHelper;
import edu.kulikov.view_model.RatingPoint;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import java.security.Principal;
import java.util.ArrayList;
import java.util.EnumSet;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static java.util.stream.Collectors.*;

@Controller
@RequestMapping("/movies")
public class MovieController {

    private static final int MOVIES_PER_PAGE = 15;

    @Autowired
    private MovieService movieService;
    @Autowired
    private RatingService ratingService;
    @Autowired
    private UserService userService;

    @RequestMapping(value = {"", "/"}, method = RequestMethod.GET)
    public ModelAndView getAll(@RequestParam(required = false) String page) {
        Long moviesCount = movieService.getCount();
        PaginationHelper helper =
                new PaginationHelper(ValidationHelper.checkPage(page), MOVIES_PER_PAGE, moviesCount);
        List<Movie> movies = movieService.getAll(MOVIES_PER_PAGE,
                helper.getOffset(), EnumSet.of(Movie.LAZY.GENRES));
        ModelAndView modelAndView = new ModelAndView("movie/movies", "movies", movies);
        modelAndView.addObject("pages", helper.getPagination(10));
        return modelAndView;
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public ModelAndView getById(@PathVariable String id,
                                Principal principal) {
        Integer movieId = ValidationHelper.checkId(id);
        Movie movie = movieService.getById(movieId, EnumSet.allOf(Movie.LAZY.class));
        if (movie == null) {
            return new ModelAndView("redirect:/movies");
        }

        ModelAndView modelAndView = new ModelAndView("movie/movie", "movie", movie);

        Map<Integer, Long> ratings = movie.getRatings().stream()
                .collect(groupingBy(Rating::getStars, counting()));
        System.out.println(principal == null);
        if (principal != null) {
            modelAndView.addObject("rating_points", RatingPoint.createFromMapCount(ratings));
            User user = userService.getByLogin(principal.getName(), null);
            if (user != null) {
                Rating rating = ratingService.getRating(user.getId(), movie.getId());
                if (rating != null) {
                    modelAndView.addObject("stars", rating.getStars());
                }
            }
        }

        return modelAndView;
    }

}
