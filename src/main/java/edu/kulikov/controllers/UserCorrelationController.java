package edu.kulikov.controllers;

import edu.kulikov.models.UserCorrelation;
import edu.kulikov.services.UserCorrelationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;


@Controller
@RequestMapping("/correlations")
public class UserCorrelationController {
    @Autowired
    private UserCorrelationService correlationService;

    @RequestMapping(value = "top")
    @ResponseBody
    public List<UserCorrelation> getTopCorrelations(@RequestParam Integer userID) {
        List<UserCorrelation> topNbyUser = correlationService.getTopNbyUser(userID, false);
        return topNbyUser;
    }


}
