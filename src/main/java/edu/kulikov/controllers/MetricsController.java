package edu.kulikov.controllers;

import edu.kulikov.models.Metric;
import edu.kulikov.services.MetricService;
import edu.kulikov.services.MetricServiceExecutor;
import edu.kulikov.services.UserService;
import edu.kulikov.support.web.MessageHelper;
import edu.kulikov.view_model.MetricForm;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.validation.Valid;
import java.util.List;

@Controller
@RequestMapping("/metrics")
public class MetricsController {
    @Autowired
    private MetricServiceExecutor executor;
    @Autowired
    private MetricService metricService;
    @Autowired
    private UserService userService;

    @RequestMapping({"", "/"})
    public ModelAndView getAll() {
        List<Metric> metrics = metricService.getAll();
        ModelAndView modelAndView = new ModelAndView("metric/metrics", "metrics", metrics);
        return modelAndView;
    }

    @RequestMapping(value = "/add", method = RequestMethod.POST)
    public String startTest(@Valid @ModelAttribute MetricForm metricForm, Errors errors,
                            final RedirectAttributes ra) throws Exception {
        if (errors.hasErrors()) {
            return "metric/metric_new";
        }
        if (userService.getById(metricForm.getUserId(), null) == null) {
            errors.rejectValue("userId", null, "Пользователя с таким логином не сущетсвует!");
            return "metric/metric_new";
        }
        if (metricForm.getStart() > metricForm.getEnd()) {
            errors.rejectValue("start", null, "Начальное значение не может быть больше конечного");
            errors.rejectValue("end", null, "Конечное значение не может быть меньше начального");
            return "metric/metric_new";
        }


        MessageHelper.addSuccessAttribute(ra, String.format("Тест добавлен в очередь:  пользователь №%d, " +
                "тренировочная выборка %d%%", metricForm.getUserId(), metricForm.getSplitter()));
        executor.addToQueue(metricForm);

        return "redirect:/metrics";
    }

    @RequestMapping(value = "/add", method = RequestMethod.GET)
    public String createMetricTest(Model model) {
        model.addAttribute(new MetricForm());
        return "metric/metric_new";
    }

}
