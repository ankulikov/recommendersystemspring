package edu.kulikov.controllers;

import edu.kulikov.dao.AccountRepository;
import edu.kulikov.models.Account;
import edu.kulikov.models.User;
import edu.kulikov.services.AccountService;
import edu.kulikov.services.UserService;
import edu.kulikov.view_model.SignupForm;
import edu.kulikov.support.web.MessageHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import java.security.Principal;

@Controller
public class SignupController {

    private static final String SIGNUP_VIEW_NAME = "signup/signup";

    @Autowired
    private AccountRepository accountRepository;

    @Autowired
    private AccountService accountService;
    @Autowired
    private UserService userService;

    @RequestMapping(value = "signup")
    public String signup(Model model, HttpServletRequest request, Principal principal) {
        model.addAttribute(new SignupForm());
        String referer = request.getHeader("Referer");
        request.getSession().setAttribute("url_prior_registration", referer);
        if (principal!=null) return "home/home";
        return SIGNUP_VIEW_NAME;
    }

    @RequestMapping(value = "signup", method = RequestMethod.POST)
    @Transactional
    public String signup(@Valid @ModelAttribute SignupForm signupForm, Errors errors, RedirectAttributes ra,
                         HttpServletRequest request) {
        if (errors.hasErrors()) {
            return SIGNUP_VIEW_NAME;
        }
        Account account = signupForm.createAccount();

        //System.out.println(user);
        if (accountService.getByLogin(account.getLogin()) != null) {
            errors.rejectValue("login", "login.notUnique");
            return SIGNUP_VIEW_NAME;
        }
        HttpSession session = request.getSession();
        String redirect = null;
        if (session != null) {
            redirect = (String) session.getAttribute("url_prior_registration");
            System.out.println(redirect);
            if (redirect != null)
                session.removeAttribute("url_prior_registration");
        }
        User user = new User(signupForm.getAge(), signupForm.isMale(), signupForm.getOccupation());
        userService.save(user);
        account.setUser(user);
        Account savedAccount = accountRepository.save(account);
        accountService.signin(savedAccount);
        MessageHelper.addSuccessAttribute(ra, "signup.success");
        return "redirect:" + (redirect == null ? "/" : redirect);
    }
}
