package edu.kulikov.controllers;


import edu.kulikov.bayes.BayesDataCreator;
import edu.kulikov.dao.MovieRepository;
import edu.kulikov.dao.TestRepository;
import edu.kulikov.dao.UserCorrelationRepository;
import edu.kulikov.dao.UserRepository;
import edu.kulikov.models.*;
import edu.kulikov.services.*;
import edu.kulikov.view_model.DonutPoint;
import org.apache.commons.lang3.tuple.Pair;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.util.Assert;
import org.springframework.web.bind.annotation.*;
import weka.core.Instances;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;
import java.util.stream.Collectors;

@SuppressWarnings("SpringJavaAutowiringInspection")
@Controller
public class TestController {
    private MovieRepository movieRepository;
    private UserService userService;
    private MovieService movieService;
    private BayesDataCreator bayesDataCreator;
    private BayesClassifierService bayesClassifierService;
    private UserCorrelationRepository userCorrelationRepository;
    private UserCorrelationService userCorrelationService;
    private RatingService ratingService;
    private PredictionService predictionService;
    private TestRepository testRepository;
    //private AccountService accountService;


    @Autowired
    public TestController(MovieRepository movieRepository, UserService userService,
                          MovieService movieService, BayesDataCreator bayesDataCreator, BayesClassifierService bayesClassifierService, UserCorrelationRepository userCorrelationRepository, UserCorrelationService userCorrelationService, RatingService ratingService, PredictionService predictionService, TestRepository testRepository) {
        this.movieRepository = movieRepository;
        this.userService = userService;
        this.movieService = movieService;
        this.bayesDataCreator = bayesDataCreator;
        this.bayesClassifierService = bayesClassifierService;
        this.userCorrelationRepository = userCorrelationRepository;
        this.userCorrelationService = userCorrelationService;
        this.ratingService = ratingService;
        this.predictionService = predictionService;
        this.testRepository = testRepository;
    }

    @RequestMapping(value = "movie/{movieId}", method = RequestMethod.GET)
    @ResponseBody
    public Movie getMovie(@PathVariable Integer movieId) {
        Assert.notNull(movieRepository);
        return movieRepository.getById(movieId, EnumSet.allOf(Movie.LAZY.class));
    }

    @RequestMapping(value = "user/{userId}", method = RequestMethod.GET)
    @ResponseBody
    public User getUser(@PathVariable Integer userId) {
        Assert.notNull(userService);
        return userService.getById(userId, EnumSet.allOf(User.LAZY.class));
    }

    @RequestMapping(value = "rating/{userId}", method = RequestMethod.GET)
    @ResponseBody
    public List<Rating> getRating(@PathVariable Integer userId) {
        Assert.notNull(userService);
        User byIdLight = userService.getById(userId, EnumSet.allOf(User.LAZY.class));
        return byIdLight.getRatings();
    }

    @RequestMapping(value = "write_train_dataset/{userId}", method = RequestMethod.GET)
    @ResponseBody
    public String writeBayesTrainDataset(@PathVariable Integer userId) throws IOException {
        File file = bayesDataCreator.exportRatedMovies(userId, true);
        return file.toString();
    }

    @RequestMapping(value = "write_test_dataset/{movieId}", method = RequestMethod.GET)
    @ResponseBody
    public String writeBayesTestDataset(@PathVariable Integer movieId) throws IOException {
        File file = bayesDataCreator.exportUnratedMovie(movieId);
        return file.toString();
    }


    @RequestMapping(value = "get_new_distribution", method = RequestMethod.GET)
    @ResponseBody
    public HashMap<String, Double> getNewClassesDistribution(@RequestParam Integer user,
                                                             @RequestParam Integer movie) throws Exception {
        Instances trainingSet = bayesClassifierService.loadTrainingSet(user, true);
        bayesClassifierService.trainClassifier(trainingSet);
        Instances testSet = bayesClassifierService.loadTestSet(movie);
        return bayesClassifierService.getClassesPrediction(testSet);
    }

    @RequestMapping(value = "save_distribution", method = RequestMethod.GET)
    @ResponseBody
    public HashMap<String, Double> saveDistribution(@RequestParam Integer user,
                                                    @RequestParam Integer movie) throws Exception {
        Instances trainingSet = bayesClassifierService.loadTrainingSet(user, true);
        bayesClassifierService.trainClassifier(trainingSet);
        Instances testSet = bayesClassifierService.loadTestSet(movie);
        HashMap<String, Double> distribution = bayesClassifierService.getClassesPrediction(testSet);
        Prediction prediction = predictionService.getPredictionByDistribution(user, movie, distribution);
        predictionService.save(prediction);

        return distribution;
    }


    @RequestMapping(value = "get_user_correlation", method = RequestMethod.GET)
    @ResponseBody
    public double getUserCorrelation(@RequestParam Integer id1,
                                     @RequestParam Integer id2) {
        User user1 = userService.getById(id1, EnumSet.noneOf(User.LAZY.class));
        User user2 = userService.getById(id2, EnumSet.noneOf(User.LAZY.class));
        UserCorrelation correlation = userCorrelationRepository.getByTwoUsers(user1, user2);
        return correlation.getCorrelation();
    }

    @RequestMapping(value = "get_user_correlations", method = RequestMethod.GET)
    @ResponseBody
    public List<Integer> getUserCorrelations(@RequestParam Integer id) {
        User user = userService.getById(id, EnumSet.noneOf(User.LAZY.class));
        List<UserCorrelation> correlation = userCorrelationRepository.getAllByUser(user);
        List<Integer> collect = correlation.stream().map(uc -> uc.getUser2().getId()).collect(Collectors.toList());
        return collect;
    }

    @RequestMapping(value = "compute_user_correlation", method = RequestMethod.GET)
    @ResponseBody
    public UserCorrelation computeUsersCorrelation(@RequestParam Integer id1,
                                                   @RequestParam Integer id2) {
        List<Pair<Integer, Byte>> stars1 = predictionService.getAllPseudoStars(id1);
        List<Pair<Integer, Byte>> stars2 = predictionService.getAllPseudoStars(id2);

        double[] doubleStars1 = stars1.stream().sorted((a, b) -> Integer.compare(a.getLeft(), b.getLeft()))
                .mapToDouble(Pair::getRight).toArray();
        double[] doubleStars2 = stars2.stream().sorted((a, b) -> Integer.compare(a.getLeft(), b.getLeft()))
                .mapToDouble(Pair::getRight).toArray();

        UserCorrelation correlation = userCorrelationService.computeUserCorrelation(id1, id2, doubleStars1, doubleStars2);
        return correlation;

    }

    @RequestMapping(value = "get_common_ratings", method = RequestMethod.GET)
    @ResponseBody
    public Pair<List<Rating>, List<Rating>> getCommonMoviesRatings(@RequestParam Integer id1,
                                                                   @RequestParam Integer id2) {
        return ratingService.getCommonRatedMoviesRatings(id1, id2);
    }

    @RequestMapping(value = "add_user_correlation", method = RequestMethod.GET)
    @ResponseBody
    public void addUserCorrelation(@RequestParam Integer userID1,
                                   @RequestParam Integer userID2,
                                   @RequestParam Double value) {
        UserCorrelation userCorrelation = new UserCorrelation();
        User user1 = userService.getById(userID1, EnumSet.noneOf(User.LAZY.class));
        User user2 = userService.getById(userID2, EnumSet.noneOf(User.LAZY.class));
        userCorrelation.setUser1(user1);
        userCorrelation.setUser2(user2);
        userCorrelation.setCorrelation(value);
        userCorrelationService.save(userCorrelation);
    }


    @RequestMapping(value = "get_pseudo_rating", method = RequestMethod.GET)
    @ResponseBody
    public Integer getPseudoRating(@RequestParam Integer userID,
                                   @RequestParam Integer movieID) {
        Integer stars = predictionService.getPseudoStars(userID, movieID);
        return stars;
    }

    @RequestMapping(value = "get_avg_pseudo_rating", method = RequestMethod.GET)
    @ResponseBody
    public Double getPseudoRating(@RequestParam Integer userID) {
        return predictionService.getAveragePseudoStarsForUser(userID);
    }

    @RequestMapping(value = "get_unrated_movies_ids", method = RequestMethod.GET)
    @ResponseBody
    public int[] getUnratedMovies(@RequestParam Integer userID) {
        return movieService.getUnratedMovies(userID).stream().mapToInt(Movie::getId).toArray();
    }

    @RequestMapping(value = "check_principal", method = RequestMethod.GET)
    @ResponseBody
    public Object checkPrincipal() {
        return SecurityContextHolder.getContext().getAuthentication();
    }

    @RequestMapping(value = "correlation_outdated", method = RequestMethod.GET)
    @ResponseStatus(HttpStatus.OK)
    public void setCorrelationOutdated() {
        userCorrelationService.setCorrelationsOutdated(1);
    }

    @RequestMapping(value = "remove_prediction", method = RequestMethod.GET)
    @ResponseStatus(HttpStatus.OK)
    public void removePrediction(@RequestParam Integer userID,
                                 @RequestParam Integer movieID) {
        User user = userService.getById(userID, EnumSet.noneOf(User.LAZY.class));
        Movie movie = movieService.getById(movieID, EnumSet.noneOf(Movie.LAZY.class));
        Prediction prediction = new Prediction();
        prediction.setUser(user);
        prediction.setMovie(movie);
        predictionService.delete(prediction);
    }

    @RequestMapping(value = "content_prediction_outdated", method = RequestMethod.GET)
    @ResponseStatus(HttpStatus.OK)
    public void setContentPredictionOutdated(@RequestParam Integer userID) {
        predictionService.setContentPredictionOutdatedForUser(userID);
    }

    @RequestMapping(value = "get_all_occupations_share_by_stars", method = RequestMethod.GET)
    @ResponseBody
    public List<DonutPoint> getAllOccupationsShareByStars(@RequestParam Integer stars) {
        return ratingService.getAllOccupationsShareByStars(stars).stream()
                .map(m -> new DonutPoint(m.getLeft(), m.getRight()))
                .collect(Collectors.toList());
    }

    @RequestMapping(value = "movie_initialize", method = RequestMethod.GET)
    @ResponseBody
    public List<Genre> initMovie(@RequestParam Integer movieId) {
        Movie movie = movieService.getById(movieId, null);
        movieRepository.initialize(movie, EnumSet.of(Movie.LAZY.GENRES));
        return movie.getGenres();
    }

    @RequestMapping(value = "get_all_pseudo_stars", method = RequestMethod.GET)
    @ResponseBody
    public List<Pair<Integer, Byte>> getAllPseudoStars(@RequestParam Integer userId) {
        return predictionService.getAllPseudoStars(userId);
    }

    @RequestMapping(value = "delete_user", method = RequestMethod.GET)
    @ResponseStatus(HttpStatus.OK)
    public void deleteUser(@RequestParam Integer userId) {
        userService.delete(userId);
    }

    @RequestMapping(value="get_test", method = RequestMethod.GET)
    @ResponseBody
    public TestModel getTest(@RequestParam Integer id) {
        return testRepository.getById(id);
    }

    @RequestMapping(value = "add_test")
    @ResponseStatus(HttpStatus.OK)
    public void addTest() {
        TestModel testModel = new TestModel();
        testModel.fpr = Arrays.asList(0.32, 0.67, 0.24, 0.49);
        testModel.tpr = Arrays.asList(1.23, 9.342423, 0.21313, 0.99);
        testModel.threshold = Arrays.asList(5,3,10,3);
        testRepository.save(testModel);
    }


//    @RequestMapping(value = "get_current_user", method = RequestMethod.GET)
//    @ResponseBody
//    public User getCurrentUser(HttpServletRequest request, Principal principal) {
//        if (!request.isUserInRole("ROLE_ANONYMOUS")) {
//            Account account = accountService.getByLogin(principal.getName());
//            accountService.initialize(account, EnumSet.of(Account.LAZY.USER));
//            return account.getUser();
//        }
//        return null;
//    }


}
