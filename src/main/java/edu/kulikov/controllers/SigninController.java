package edu.kulikov.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.servlet.http.HttpServletRequest;
import java.security.Principal;

@Controller
public class SigninController {

	@RequestMapping(value = "signin", method = RequestMethod.GET)
	public String signin(HttpServletRequest request, Principal principal) {
        String referer = request.getHeader("Referer");
        request.getSession().setAttribute("url_prior_login", referer);
        System.out.println(referer);
        if (principal !=null) return "home/home";
        return "signin/signin";
    }
}
