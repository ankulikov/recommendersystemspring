package edu.kulikov.controllers;

import edu.kulikov.models.Rating;
import edu.kulikov.models.User;
import edu.kulikov.services.PredictionService;
import edu.kulikov.services.UserService;
import edu.kulikov.support.web.PaginationHelper;
import edu.kulikov.support.web.ValidationHelper;
import edu.kulikov.view_model.MovieTitleStars;
import edu.kulikov.view_model.RatingPoint;
import edu.kulikov.view_model.UserWithRatings;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static java.util.stream.Collectors.counting;
import static java.util.stream.Collectors.groupingBy;

@Controller
@RequestMapping("/users")
public class UserController {
    private static final int USERS_PER_PAGE = 20;

    @Autowired
    private UserService userService;
    @Autowired
    private PredictionService predictionService;

    @RequestMapping(value = {"", "/"}, method = RequestMethod.GET)
    public ModelAndView getAll(@RequestParam(required = false) String page) {
        Long usersCount = userService.getCount();
        PaginationHelper helper = new PaginationHelper(
                ValidationHelper.checkPage(page), USERS_PER_PAGE, usersCount);

        List<User> users = userService.getAll(USERS_PER_PAGE,
                helper.getOffset(), EnumSet.allOf(User.LAZY.class));

        List<UserWithRatings> usersWithRatings = new ArrayList<>();

        for (User user : users) {
            usersWithRatings.add(
                    new UserWithRatings(
                            user,
                            user.getRatings()
                                    .stream()
                                    .mapToInt(Rating::getStars)
                                    .average()
                                    .orElse(0),
                            user.getRatings().size()
                    )
            );
        }

        ModelAndView modelAndView = new ModelAndView("user/users", "usersWithRatings", usersWithRatings);
        modelAndView.addObject("pages", helper.getPagination(10));

        return modelAndView;
    }

    @RequestMapping(value = "/{id:[\\d]+}", method = RequestMethod.GET)
    public ModelAndView getById(@PathVariable String id) {
        Integer userId = ValidationHelper.checkId(id);

        User user = userService.getById(userId, EnumSet.of(User.LAZY.RATINGS));
        if (user == null)
            return new ModelAndView("redirect:/users");

        List<Rating> allRatings = user.getRatings();
        Map<Integer, Long> ratings = allRatings.stream()
                .collect(groupingBy(Rating::getStars, counting()));
        List<MovieTitleStars> moviesStars = allRatings
                .stream()
                .map(r -> new MovieTitleStars(r.getMovie().getId(), r.getMovie().getTitle(), r.getStars()))
                .sorted((a, b) -> Integer.compare(b.stars, a.stars))
                .collect(Collectors.toList());
        ModelAndView modelAndView = new ModelAndView("user/user", "user", user);
        modelAndView.addObject("rating_points", RatingPoint.createFromMapCount(ratings));
        modelAndView.addObject("movies_stars", moviesStars);
        if (user.isRecommended()) {
            modelAndView.addObject("recommendations",
                    predictionService.getTopNPredictionsForUser(userId, 15));
        }
        return modelAndView;
    }

    @RequestMapping(value = "/{login:[^\\d]{1}.*}", method = RequestMethod.GET)
    public ModelAndView getByLogin(@PathVariable String login) {
        User user = userService.getByLogin(login, EnumSet.of(User.LAZY.RATINGS));
        if (user == null)
            return new ModelAndView("redirect:/users");
        return getById(String.valueOf(user.getId()));
    }


}
