# Collaborative Filtering Recommender System for Web-based Applications #

System that makes predictions for movies that you would like to watch using naive bayes classifier and collaborative filtering algorithm.  

### How do I get set up? ###

* Setup MySQL database server, create database **recommender_system** and import all tables from [these scripts](https://yadi.sk/d/8gynOlQlgf9Hk)
* Download and extract server WildFly (version >= 8) in any forlder
* Download [this artifact](https://yadi.sk/d/zsNhgiMegf9wm), open it as an archive, edit file **\WEB-INF\classes\system.properties**: specify url (**dataSource.url**), username (**dataSource.username**) and password (**dataSource.password**) to MySQL server instance and put the artifact in the directory **standalone\deployments** relatively your folder with server
* Start WildFly server by executing file **standalone.bat** (or **standalone.sh**). If everything is well the artifact will be autodeployed. 
* Open page [http://localhost:8080/RecommenderSystem](http://localhost:8080/RecommenderSystem)
* ...
* PROFIT!

### Sceenshots ###
 ![Home page](https://bitbucket.org/repo/xjGXzK/images/3394757588-homepage.png) 
 ![Registration](https://bitbucket.org/repo/xjGXzK/images/1272683043-registration.png)
 ![Authorization](https://bitbucket.org/repo/xjGXzK/images/2006314658-auth.png)
 ![All movies](https://bitbucket.org/repo/xjGXzK/images/3879655848-allmovies.png)
 ![Movie](https://bitbucket.org/repo/xjGXzK/images/483140420-movie.png)
 ![User](https://bitbucket.org/repo/xjGXzK/images/2312289150-user.png)
 ![recommendations](https://bitbucket.org/repo/xjGXzK/images/2304806141-recommendations.png)
 ![Metrics](https://bitbucket.org/repo/xjGXzK/images/1009545003-metrics.png)

### Who do I talk to? ###

* Author: [Kulikov Andrey](https://vk.com/andreyhse)